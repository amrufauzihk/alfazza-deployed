import {
  IonApp,
  IonRouterOutlet,
  IonSplitPane,
  setupIonicReact,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import { Redirect, Route } from "react-router-dom";

import "./index.css";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import Menu from "./common/Menu";
import Sales from "./pages/Sales";
import Profile from "./pages/Profile";
import Expense from "./pages/Expense";
import Receipts from "./pages/Receipts";
import Investor from "./pages/Investor";
import Purchase from "./pages/Purchase";
import Category from "./pages/Category";

/* Theme variables */
// import './theme/variables.css';

setupIonicReact();

const App: React.FC = () => {
  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main" style={{ background: "#f3f3f3" }}>
            <Route path="/" exact={true}>
              <Redirect to="/dashboard" />
            </Route>
            {/* <Route component={Page} path="/folder/:name" exact={true} /> */}
            <Route component={Login} path="/login" exact={true} />
            <Route path="/dashboard" exact={true}>
              <Dashboard />
            </Route>
            <Route path="/sales" exact={true}>
              <Sales />
            </Route>
            <Route path="/purchase" exact={true}>
              <Purchase />
            </Route>
            <Route path="/profile" exact={true}>
              <Profile />
            </Route>
            <Route path="/expense" exact={true}>
              <Expense />
            </Route>
            <Route path="/receipt" exact={true}>
              <Receipts />
            </Route>
            <Route path="/investor" exact={true}>
              <Investor />
            </Route>
            <Route path="/category" exact={true}>
              <Category />
            </Route>
            {/* <Route path="/supplies" exact={true}>
              <Supplies />
            </Route> */}
            {/* <Route path="/assets" exact={true}>
              <Assets />
            </Route> */}
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
