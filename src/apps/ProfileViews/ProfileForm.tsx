import { IonButton, IonFooter, IonInput, IonItem, IonList, IonListHeader, IonTextarea } from '@ionic/react';
import React from 'react';

const ProfileForm: React.FC<any> = ({
  register,
  handleSubmit,
  onSubmit,
  onCancel,
}) => {
  return (
    <form onSubmit={handleSubmit((data: any) => onSubmit(data))}>
      <IonList>
        <IonItem>
          <IonInput {...register('tanggalLahir')} type='date' label="Tanggal Transaksi" labelPlacement="stacked"></IonInput>
        </IonItem>
        <IonItem>
          <IonInput {...register('name')} type='text' label="Nama" labelPlacement="stacked" placeholder="Enter text"></IonInput>
        </IonItem>
        <IonItem>
          <IonInput {...register('name')} type='text' label="Nama" labelPlacement="stacked" placeholder="Enter text"></IonInput>
        </IonItem>
        <IonItem>
          <IonInput {...register('name')} type='text' label="Nama" labelPlacement="stacked" placeholder="Enter text"></IonInput>
        </IonItem>
        <IonItem>
          <IonInput {...register('name')} type='text' label="Nama" labelPlacement="stacked" placeholder="Enter text"></IonInput>
        </IonItem>
      </IonList>
      <div className='d-flex ion-justify-content-end'>
        <IonButton onClick={onCancel} color={`danger`}>Cancel</IonButton>
        <IonButton type='submit'>Update</IonButton>
      </div>
    </form>
  );
};

export default ProfileForm;