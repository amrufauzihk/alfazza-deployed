import { IonAvatar, IonButton, IonButtons, IonCard, IonCardContent, IonContent, IonHeader, IonIcon, IonImg, IonItem, IonLabel, IonList, IonModal, IonPage, IonTitle, IonToolbar, useIonToast } from '@ionic/react';
import { closeOutline, pencil, trashOutline } from 'ionicons/icons';
import React, { useState } from 'react';
import MainHeaders from '../../common/MainHeaders';
import ProfileForm from './ProfileForm';
import { useForm } from 'react-hook-form';

const ProfileViews: React.FC = () => {
  const [showModal, setShowModal] = useState<boolean>(false);
  const {
    register,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const [present] = useIonToast();

  const onCancel = () => {
    // logic should be here
    setShowModal(false)
    present({
      message: '[MOCK] Dicoba Error saja',
      duration: 1500,
      position: 'top',
      color: 'danger'
    });
  }
  const onSubmit = () => {
    // logic should be here
    setShowModal(false)
    present({
      message: 'Berhasil diubah',
      duration: 1500,
      position: 'top',
      color: 'success'
    });
  }

  return (
    <IonPage>
      <MainHeaders title='Profile' />
      <IonContent className="ion-padding">
        <IonCard>
          <IonCardContent>
            <div className="image-profile d-flex ion-justify-content-center" style={{ maxHeight: '200px', maxWidth: '100%' }}>
              {/* <IonAvatar> */}
                <IonImg src="https://karasubloodseeker.files.wordpress.com/2012/06/hitsugaya-toushiro.jpg" alt="Foto Profil" />
              {/* </IonAvatar> */}
            </div>
            <IonList>
              <IonItem>
                <IonLabel>Nama:</IonLabel>
                <IonLabel slot="end">John Doe</IonLabel>
              </IonItem>
              <IonItem>
                <IonLabel>Tempat Tanggal Lahir:</IonLabel>
                <IonLabel slot="end">Jakarta, 01 Januari 1990</IonLabel>
              </IonItem>
              <IonItem>
                <IonLabel>Asal Sekolah:</IonLabel>
                <IonLabel slot="end">SMA Negeri 1 Jakarta</IonLabel>
              </IonItem>
              <IonItem>
                <IonLabel>Jenis Kelamin:</IonLabel>
                <IonLabel slot="end">Laki-laki</IonLabel>
              </IonItem>
              <IonButton expand="full" onClick={() => setShowModal(true)}>
                <IonIcon slot="start" icon={pencil} />
                Edit Profil
              </IonButton>
            </IonList>
          </IonCardContent>
        </IonCard>
      </IonContent>
      <IonModal mode="ios" isOpen={showModal} onDidDismiss={() => setShowModal(false)}>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot='end'>
              <IonButton onClick={() => setShowModal(false)}>
                <IonIcon icon={closeOutline} />
              </IonButton>
            </IonButtons>
          </IonToolbar>
        </IonHeader>
        <IonContent className='ion-padding'>
          <ProfileForm
            handleSubmit={handleSubmit} register={register} onCancel={onCancel} onSubmit={onSubmit} 
          />
        </IonContent>
      </IonModal>
    </IonPage>
  );
};

export default ProfileViews;
