import { IonButton, IonCard, IonContent, IonHeader, IonInput, IonItem, IonList, IonPage, IonSelect, IonSelectOption, IonTextarea, IonTitle, IonToolbar, useIonLoading, useIonToast } from '@ionic/react';
import React from 'react';
import MainHeaders from '../../common/MainHeaders';
import { useForm } from 'react-hook-form';

import './ExpenseViews.css';
import { createData } from '../../firebase/firebaseService';

const ExpenseViews: React.FC = () => {
  const [showIonLoading, dismissIonLoading] = useIonLoading();
  const [present] = useIonToast();

  const {
    register,
    setValue,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm();

  const handleCancel = () => {
    // something happens here
    /** ONLY MOCK: TECHDEBT */
    present({
      message: 'Contoh Kalau Gagal menambahkan',
      duration: 1500,
      position: 'top',
      color: 'danger'
    });
  }
  
  const onSubmit = async (data: any) => {
    try {
      Object.keys(data).forEach((key) => {
        if (data[key] === undefined) {
          delete data[key];
        }
      });
      showIonLoading('Loading')
      await createData(data, 'expense');
  
      dismissIonLoading();
      present({
        message: 'Berhasil ditambahkan',
        duration: 1500,
        position: 'top',
        color: 'success'
      });
    } catch (error) {
      console.log('error', error);
      present({
        message: 'Gagal Menambahkan data, Terjadi Kesalahan',
        duration: 1500,
        position: 'top',
        color: 'danger'
      });
    } finally {
      // handleCloseDetailModal();
      // fetchInventoryList();
    }
  }


  return (
    <IonPage>
      <MainHeaders title='Expense' />
      <IonContent className="ion-padding">
        <form
          onSubmit={handleSubmit((data: any) => onSubmit(data))}
        >
          <IonCard>
            <IonList>
              <div className='ion-padding-start ion-padding-top'>
                <IonInput {...register('tanggalPembayaran')} type='date' label="Tanggal Pembayaran" labelPlacement="stacked"></IonInput>
              </div>
              <div className='ion-padding-start ion-padding-top'>
                {/* <IonInput {...register('noAkad')} type='text' label="No. Akad" labelPlacement="stacked" placeholder="Enter text"></IonInput> */}
                <IonSelect {...register('jenisPembayaran')} label="Stacked label" labelPlacement="stacked" placeholder="Pilih Jenis Pembayaran">
                  <IonSelectOption value="gaji">gaji</IonSelectOption>
                  <IonSelectOption value="thr">thr</IonSelectOption>
                  <IonSelectOption value="tujangan komunikasi">tujangan komunikasi</IonSelectOption>
                  <IonSelectOption value="tunjangan transportasi">tunjangan transportasi</IonSelectOption>
                  <IonSelectOption value="biaya meeting">biaya meeting</IonSelectOption>
                  <IonSelectOption value="listrik">listrik</IonSelectOption>
                  <IonSelectOption value="biaya sewa kantor">biaya sewa kantor</IonSelectOption>
                  <IonSelectOption value="administrasi kantor">administrasi kantor</IonSelectOption>
                  <IonSelectOption value="administrasi bank">administrasi bank</IonSelectOption>
                  <IonSelectOption value="dividen">dividen</IonSelectOption>
                  <IonSelectOption value="zakat">zakat</IonSelectOption>
                </IonSelect>
              </div>
              <div className='ion-padding-start ion-padding-top'>
                <IonInput {...register('totalPembayaran')} type='number' label="Total Pembayaran" labelPlacement="stacked" placeholder="Enter text"></IonInput>
              </div>
              <div className='ion-padding-start ion-padding-top'>
                <IonTextarea {...register('keterangan')} className='' label="Keterangan" labelPlacement="stacked" placeholder="Enter text"></IonTextarea>
              </div>
            </IonList>
            <div className='d-flex ion-justify-content-end ion-padding'>
              <IonButton onClick={handleCancel} color={`danger`}>Cancel</IonButton>
              <IonButton type='submit'>Tambahkan Data</IonButton>
            </div>
          </IonCard>
        </form>
      </IonContent>
    </IonPage>
  );
};

export default ExpenseViews;