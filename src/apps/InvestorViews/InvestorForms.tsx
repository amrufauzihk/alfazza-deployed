import { IonList, IonListHeader, IonItem, IonInput, IonTextarea, IonLabel, IonText, IonItemGroup, IonButton, IonNote } from '@ionic/react';
import React, { useState } from 'react';
import Radio from '../../common/Radio';
// import { ErrorMessage } from "@hookform/error-message"

const InvestorForms: React.FC<any> = ({
  register,
  handleSubmit,
  onSubmit,
  onCancel,
  errors,
  getValues,
}) => {
  // const [selectedPurchase, setSelectedPurchase] = useState('Inventory');
  // console.log('errors 123', errors)

  return (
    <form
      onSubmit={handleSubmit((data: any) => onSubmit(data))}
      // onChange={() => setSelectedPurchase(getValues()?.kindOfTransaction || 'Inventory')}
    >
      <IonList>
        <IonInput
          required
          {...register('namaInvestor')}
          type='text'
          label="Nama Investor"
          labelPlacement="stacked"
          placeholder="Enter text"
        />
        <IonInput
          required
          {...register('alamat')}
          type='text'
          label="Alamat"
          labelPlacement="stacked"
          placeholder="Enter text"
        />
        <IonInput
          required
          {...register('jumlahInvestasi')}
          type='text'
          label="Jumlah Investasi"
          labelPlacement="stacked"
          placeholder="Enter text"
        />
        <IonInput
          required
          {...register('persentaseKepemilikan')}
          type='text'
          label="Persentase Kepemilikan (%)"
          labelPlacement="stacked"
          placeholder="Enter text"
        />
        <IonInput
          required
          {...register('tglInvestasi')}
          type='date'
          label="Tanggal Investasi"
          labelPlacement="stacked"
          placeholder="Enter text"
        />
      </IonList>
      <div className='d-flex ion-justify-content-end'>
        <IonButton onClick={onCancel} color={`danger`}>Cancel</IonButton>
        <IonButton type='submit'>Submit</IonButton>
      </div>
    </form>
  );
};

export default InvestorForms;