import {
  IonAlert,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonFab,
  IonFabButton,
  IonFabList,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonLoading,
  IonModal,
  IonPage,
  IonSegment,
  IonSegmentButton,
  IonText,
  IonTitle,
  IonToolbar,
  useIonLoading,
  useIonRouter,
  useIonToast,
} from "@ionic/react";
import React, { useEffect, useMemo, useState } from "react";
import MainHeaders from "../../common/MainHeaders";
import { Box } from "@mui/material";
import {
  add,
  closeOutline,
  colorPalette,
  document,
  globe,
  pencilSharp,
} from "ionicons/icons";
import { MRT_ColumnDef } from "material-react-table";
import Table from "../../common/Table";
import InvestorForms from "./InvestorForms";
import { useForm } from "react-hook-form";

import "./InvestorViews.css";
import {
  createData,
  getAllData,
  multipleDeleteData,
  updateData,
} from "../../firebase/firebaseService";

//simple data example - Check out https://www.material-react-table.com/docs/examples/remote for a more complex example
//If using TypeScript, define the shape of your data (optional, but recommended)
interface Person {
  namaInvestor: string;
  alamat: string;
  jumlahInvestasi: string;
  persentaseKepemilikan: string;
  tglInvestasi: string;
}
const InvestorViews: React.FC = () => {
  const router = useIonRouter();
  // const [isModalOpen, setModalOpen] = useState(false);
  // const [showLoading, setShowLoading] = useState(false);
  // const [data, setData] = useState(false);
  const [isModalOpen, setModalOpen] = useState(false);
  const [showLoading, setShowLoading] = useState(false);
  const [data, setData] = useState(false);
  const [showDetailsModal, setDetailModals] = useState<boolean>(false);
  const [selectedData, setSelectedData] = useState<any>({});
  const [deteleConfirm, setDeleteConfirm] = useState<boolean>(false);
  const [selectedDeletion, setSelectedDeletion] = useState<any[]>([]);

  const [showIonLoading, dismissIonLoading] = useIonLoading();
  const [present] = useIonToast();

  const {
    register,
    setValue,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm();

  const fetchInventoryList = async () => {
    try {
      setShowLoading(true);
      const data: any = await getAllData("investor");
      setShowLoading(false);
      setData(data);
      console.log("data", data);
    } catch (error) {
      console.log("error", error);
      present({
        message: "Terjadi Kesalahan, coba reload lagi",
        duration: 1500,
        position: "top",
        color: "danger",
      });
      setShowLoading(false);
    } finally {
      setShowLoading(false);
    }
  };

  useEffect(() => {
    fetchInventoryList();
  }, []);

  //column definitions - strongly typed if you are using TypeScript (optional, but recommended)
  const columns = useMemo<MRT_ColumnDef<Person>[]>(
    () => [
      {
        accessorKey: "namaInvestor",
        header: "namaInvestor",
        enableHiding: false,
        Cell: ({ cell, column }: any) => {
          return (
            <span
              className="ion-cursor-pointer"
              onClick={() => {
                setSelectedData(cell?.row?.original);
                setDetailModals(true);
              }}
            >
              {cell?.getValue()}
            </span>
          );
        },
      },
      {
        accessorKey: "alamat",
        header: "alamat",
        filterFn: "equals",
        filterSelectOptions: [
          { text: "Inventory", value: "Inventory" },
          { text: "Supplies", value: "Supplies" },
          { text: "Assets", value: "Assets" },
        ],
        filterVariant: "select",
      },
      {
        accessorKey: "jumlahInvestasi",
        header: "jumlahInvestasi",
      },
      {
        accessorKey: "persentaseKepemilikan",
        header: "persentaseKepemilikan",
      },
      {
        accessorKey: "tglInvestasi",
        header: "tglInvestasi",
      },
    ],
    []
  );

  const handleEdit = () => {
    setModalOpen(true);
    /** SET ALL VALUE !! WE CAN DO IT AUTOMATICALLY, SO THAT SHOULD BE TECHDEBT */
    setValue("namaInvestor", selectedData?.namaInvestor);
    setValue("alamat", selectedData?.alamat);
    setValue("jumlahInvestasi", selectedData?.jumlahInvestasi);
    setValue("persentaseKepemilikan", selectedData?.persentaseKepemilikan);
    setValue("tglInvestasi", selectedData?.tglInvestasi);
  };

  const handleCancel = () => {
    // something happens here
    setModalOpen(false);
    /** ONLY MOCK: TECHDEBT */
    present({
      message: "Contoh Kalau Gagal menambahkan",
      duration: 1500,
      position: "top",
      color: "danger",
    });
  };

  const onSubmit = async (data: any) => {
    try {
      Object.keys(data).forEach((key) => {
        if (data[key] === undefined) {
          delete data[key];
        }
      });
      showIonLoading("Loading");
      selectedData?.id
        ? await updateData(selectedData.id, data, "investor")
        : await createData(data, "investor");

      dismissIonLoading();
      setModalOpen(false);
      present({
        message: "Berhasil ditambahkan",
        duration: 1500,
        position: "top",
        color: "success",
      });
    } catch (error) {
      console.log("error", error);
      present({
        message: "Gagal Menambahkan data, Terjadi Kesalahan",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      handleCloseDetailModal();
      fetchInventoryList();
    }
  };

  const handleCloseDetailModal = () => {
    setSelectedData({});
    /** SET ALL VALUE !! WE CAN DO IT AUTOMATICALLY, SO THAT SHOULD BE TECHDEBT */
    setValue("namaInvestor", selectedData?.namaInvestor);
    setValue("alamat", selectedData?.alamat);
    setValue("jumlahInvestasi", selectedData?.jumlahInvestasi);
    setValue("persentaseKepemilikan", selectedData?.persentaseKepemilikan);
    setValue("tglInvestasi", selectedData?.tglInvestasi);
    setDetailModals(false);
  };

  const handleDelete = async () => {
    /** HERE IS a FUNCTION TO DELETE THE DATA */
    try {
      showIonLoading("Loading");
      await multipleDeleteData(selectedDeletion, "investor");
      dismissIonLoading();
      setModalOpen(false);
      present({
        message: "Berhasil menghapus data",
        duration: 1500,
        position: "top",
        color: "success",
      });
    } catch (error) {
      console.log("error", error);
      present({
        message: "Gagal Menghapus data, Terjadi Kesalahan",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      setSelectedDeletion([]);
      setDeleteConfirm(false);
      handleCloseDetailModal();
      fetchInventoryList();
    }
  };

  return (
    <IonPage>
      <MainHeaders title="Pembelian" />
      <IonContent className="ion-padding">
        {/* <Table
          columns={columns}
          data={data}
          showDownload={true}
          handleDeletes={(ids: string[]) => {
            setSelectedDeletion(ids);
            setDeleteConfirm(true);
          }}
        /> */}
        <IonFab
          slot="fixed"
          vertical="bottom"
          horizontal="end"
          className="ion-padding"
          onClick={() => setModalOpen(true)}
        >
          <IonFabButton>
            <IonIcon icon={add}></IonIcon>
          </IonFabButton>
        </IonFab>
        <IonModal
          mode="ios"
          isOpen={isModalOpen}
          onDidDismiss={() => setModalOpen(false)}
        >
          <IonHeader>
            <IonToolbar>
              <IonTitle>Tambahkan Pembelian</IonTitle>
              <IonButtons slot="end">
                <IonButton onClick={() => setModalOpen(false)}>
                  <IonIcon icon={closeOutline} size="large" />
                </IonButton>
              </IonButtons>
            </IonToolbar>
          </IonHeader>
          <IonContent className="ion-padding">
            <InvestorForms
              register={register}
              onSubmit={onSubmit}
              handleCancel={handleCancel}
              handleSubmit={handleSubmit}
              errors={errors}
              getValues={getValues}
            />
          </IonContent>
        </IonModal>
      </IonContent>
      <IonModal mode="ios" isOpen={showDetailsModal} onDidDismiss={handleCloseDetailModal}>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Customer Detail</IonTitle>
            <IonButtons slot="end">
              <IonButton onClick={handleCloseDetailModal}>
                <IonIcon icon={closeOutline} size="large" />
              </IonButton>
            </IonButtons>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonItem>
            <IonButton
              onClick={handleEdit}
              className="ion-margin"
              color={`success`}
              slot="end"
            >
              <IonIcon icon={pencilSharp} />
              <IonText>Edit</IonText>
            </IonButton>
          </IonItem>
          <IonItem>
            <IonCol>namaInvestor</IonCol>
            <IonCol>{selectedData?.namaInvestor}</IonCol>
          </IonItem>
          <IonItem color={`light`}>
            <IonCol>alamat</IonCol>
            <IonCol>{selectedData?.alamat}</IonCol>
          </IonItem>
          <IonItem>
            <IonCol>jumlahInvestasi</IonCol>
            <IonCol>{selectedData?.jumlahInvestasi}</IonCol>
          </IonItem>
          <IonItem color={`light`}>
            <IonCol>persentaseKepemilikan</IonCol>
            <IonCol>{selectedData?.persentaseKepemilikan}</IonCol>
          </IonItem>
          <IonItem>
            <IonCol>tglInvestasi</IonCol>
            <IonCol>{selectedData?.tglInvestasi}</IonCol>
          </IonItem>
        </IonContent>
      </IonModal>
      {/* DELETE CONFIRMATION ALERT */}
      <IonAlert
        header="Apakah anda yakin ingin menghapus data ?"
        subHeader="Data yang dihapus tidak dapat dikembalikan lagi"
        isOpen={deteleConfirm}
        buttons={[
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              setDeleteConfirm(false);
              setSelectedDeletion([]);
            },
          },
          {
            text: "OK",
            role: "confirm",
            handler: handleDelete,
          },
        ]}
      ></IonAlert>
      <IonLoading
        cssClass="my-custom-class"
        isOpen={showLoading}
        onDidDismiss={() => setShowLoading(false)}
        message={"Please Wait..."}
      />
    </IonPage>
  );
};

export default InvestorViews;
