import {
  IonButton,
  IonFooter,
  IonInput,
  IonItem,
  IonList,
  IonListHeader,
  IonNote,
  IonTextarea,
} from "@ionic/react";
import React from "react";
import { ErrorMessage } from "@hookform/error-message";
import { InputField, TextAreaField } from "../../common/FormField";
import { Stack } from "@mui/joy";

const SalesForm: React.FC<any> = ({
  register,
  handleSubmit,
  onSubmit,
  onCancel,
  errors,
}) => {
  return (
    <form onSubmit={handleSubmit((data: any) => onSubmit(data))}>
      <div className="d-flex ion-justify-content-end ion-padding">
        <IonButton onClick={onCancel} color={`danger`}>
          Cancel
        </IonButton>
        <IonButton type="submit">Submit</IonButton>
      </div>
    </form>
  );
};

export default SalesForm;
