import React, { useEffect, useMemo, useState } from "react";
import { useForm } from "react-hook-form";
import { MRT_ColumnDef, MRT_Row } from "material-react-table";
import Table from "../../common/Table";
import MainHeaders from "../../common/MainHeaders";
import {
  IonAlert,
  IonContent,
  IonLoading,
  IonPage,
  useIonLoading,
  useIonToast,
} from "@ionic/react";
import {
  createData,
  getAllData,
  multipleDeleteData,
  updateData,
} from "../../firebase/firebaseService";
import { Button, Grid } from "@mui/joy";
import { Trash2 } from "lucide-react";
import PageHeader from "../../common/PageHeader";
import { formatCurrency } from "../../utils/formatNumber";
import { formatDate } from "../../utils/formatDate";
import ModalDetails from "./components/ModalDetails/ModalDetails";
import ModalForm from "./components/ModalForm/ModalForm";
import { INIT_CATEGORY } from "./constants";

const CategoryViews: React.FC = () => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [showLoading, setShowLoading] = useState(false);
  const [data, setData] = useState<CategoryProps[]>([]);
  const [showDetailsModal, setDetailModals] = useState<boolean>(false);
  const [selectedData, setSelectedData] = useState<CategoryProps>(INIT_CATEGORY);
  const [deteleConfirm, setDeleteConfirm] = useState<boolean>(false);
  const [selectedDeletion, setSelectedDeletion] = useState<any[]>([]);

  const {
    register,
    setValue,
    reset,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm<CategoryProps>({ defaultValues: INIT_CATEGORY });

  const [showIonLoading, dismissIonLoading] = useIonLoading();
  const [present] = useIonToast();
  
  const fetchCategoryList = async () => {
    try {
      setShowLoading(true);
      const data: any = await getAllData("category");
      setShowLoading(false);
      setData(data);
    } catch (error) {
      console.log("error", error);
      present({
        message: "Terjadi Kesalahan, coba reload lagi",
        duration: 1500,
        position: "top",
        color: "danger",
      });
      setShowLoading(false);
    } finally {
      setShowLoading(false);
    }
  };

  useEffect(() => {
    fetchCategoryList();
  }, []);

  const columns = useMemo<MRT_ColumnDef<CategoryProps>[]>(
    () => [
      {
        accessorKey: "categoryName",
        header: "Nama Kategori",
        Cell: ({ row }) => {
          return (
            <span>
              {row.original.categoryName}
            </span>
          );
        },
      }
    ],
    []
  );

  const handleEdit = () => {
    setModalOpen(true);
    /** SET ALL VALUE !! WE CAN DO IT AUTOMATICALLY, SO THAT SHOULD BE TECHDEBT */
    reset(selectedData);
  };

  const onCancel = () => {
    // something happens here
    setModalOpen(false);
    /** ONLY MOCK: TECHDEBT */
    present({
      message: "Contoh Kalau Gagal menambahkan",
      duration: 1500,
      position: "top",
      color: "danger",
    });
  };

  const onSubmit = async (data: any) => {
    /** ONLY MOCK: TECHDEBT */
    console.log("onSubmit", data);
    
    try {
      showIonLoading("Loading");
      selectedData?.id
        ? await updateData(selectedData.id, data, "category")
        : await createData(data, "category");
      setModalOpen(false);
      present({
        message: "Berhasil ditambahkan",
        duration: 1500,
        position: "top",
        color: "success",
      });
    } catch (error) {
      console.log("error", error);
      present({
        message: "Gagal Menambahkan data, Terjadi Kesalahan",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      dismissIonLoading();
      handleCloseDetailModal();
      fetchCategoryList();
    }
  };

  const onError = async (data: any) => {
    console.log("error form", data);
    present({
      message: "Lengkapi Semua Data",
      duration: 1500,
      position: "bottom",
      color: "warning",
    });
  }

  const handleCloseDetailModal = () => {
    /** SET ALL VALUE !! WE CAN DO IT AUTOMATICALLY, SO THAT SHOULD BE TECHDEBT */
    setSelectedData(INIT_CATEGORY)
    reset(INIT_CATEGORY);
    setDetailModals(false);
  };

  const handleDelete = async (rows: MRT_Row<CategoryProps>[]) => {
    console.log("selectedDeletion", selectedDeletion);
    /** HERE IS a FUNCTION TO DELETE THE DATA */
    try {
      showIonLoading("Loading");
      await multipleDeleteData(
        rows.map((v) => {
          return v.original.id || "";
        }),
        "category"
      );
      dismissIonLoading();
      setModalOpen(false);
      present({
        message: "Berhasil menghapus data",
        duration: 1500,
        position: "top",
        color: "success",
      });
    } catch (error) {
      console.log("error", error);
      present({
        message: "Gagal Menghapus data, Terjadi Kesalahan",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      setSelectedDeletion([]);
      setDeleteConfirm(false);
      handleCloseDetailModal();
      fetchCategoryList();
    }
  };
  console.log("selectedData", selectedData);
  

  return (
    <IonPage>
      <MainHeaders title="Category" />
      <IonContent className="ion-padding">
        <IonContent className="ion-padding">
          <PageHeader
            title="Category"
            description="Create Category of Purchase"
          />
          <Table<CategoryProps>
            section="category"
            columns={columns}
            data={data}
            onInsert={() => setModalOpen(true)}
            enableRowSelection
            onBodyRowClick={({ row }) => ({
              onClick: (event) => {
                event.preventDefault();
                setSelectedData(row?.original);
                setDetailModals(true);
              },
              sx: { cursor: "pointer" },
            })}
            renderCustomActions={({ row }) => {
              return (
                <Button
                  color="danger"
                  variant="plain"
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelectedDeletion([row.original.id]);
                    setDeleteConfirm(true);
                  }}
                >
                  <Trash2 />
                </Button>
              );
            }}
            renderBulkActions={({ getSelectedRowModel }) => {
              const { rows } = getSelectedRowModel();
              return (
                <Grid container gap={1}>
                  <Button
                    variant="outlined"
                    color="danger"
                    onClick={() => handleDelete(rows)}
                    startDecorator={<Trash2 />}
                  >
                    Delete
                  </Button>
                </Grid>
              );
            }}
          />
        </IonContent>
      </IonContent>
      <ModalForm
        errors={errors}
        handleSubmit={handleSubmit}
        onCancel={onCancel}
        onSubmit={onSubmit}
        onError={onError}
        setValue={setValue}
        watch={watch}
        register={register}
        setShowModal={setModalOpen}
        showModal={isModalOpen}
      />
      <ModalDetails
        data={selectedData}
        handleClose={handleCloseDetailModal}
        handleEdit={handleEdit}
        showDetail={showDetailsModal}
      />
      {/* DELETE CONFIRMATION ALERT */}
      <IonAlert
        header="Apakah anda yakin ingin menghapus data ?"
        subHeader="Data yang dihapus tidak dapat dikembalikan lagi"
        isOpen={deteleConfirm}
        buttons={[
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              setDeleteConfirm(false);
              setSelectedDeletion([]);
            },
          },
          {
            text: "OK",
            role: "confirm",
            handler: handleDelete,
          },
        ]}
      ></IonAlert>
      <IonLoading
        cssClass="my-custom-class"
        isOpen={showLoading}
        onDidDismiss={() => setShowLoading(false)}
        message={"Please Wait..."}
      />
    </IonPage>
  );
};

export default CategoryViews;
