import {
  IonButton,
  IonButtons,
  IonContent,
  IonFooter,
  IonHeader,
  IonIcon,
  IonModal,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import { closeOutline, watch } from "ionicons/icons";
import {
  FieldErrors,
  UseFormHandleSubmit,
  UseFormRegister,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import { Stack } from "@mui/joy";
import {
  CurrencyField,
  InputField,
  TextAreaField,
} from "../../../../common/FormField";

type ModalFormProps = {
  showModal: boolean;
  setShowModal: (e: boolean) => void;
  handleSubmit: UseFormHandleSubmit<CategoryProps, undefined>;
  register: UseFormRegister<CategoryProps>;
  onCancel: () => void;
  onSubmit: (data: any) => Promise<void>;
  onError: (data: any) => Promise<void>;
  watch: UseFormWatch<CategoryProps>
  setValue: UseFormSetValue<CategoryProps>;
  errors: FieldErrors<CategoryProps>;
};

const ModalForm: React.FC<ModalFormProps> = ({
  showModal,
  setShowModal,
  handleSubmit,
  register,
  onCancel,
  onSubmit,
  onError,
  watch,
  setValue,
  errors,
}) => {
  return (
    <IonModal mode="ios" isOpen={showModal} onDidDismiss={() => setShowModal(false)}>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tambahkan Data</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={() => setShowModal(false)}>
              <IonIcon icon={closeOutline} size="large" />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <Stack display="flex" flexDirection="column" gap={2} p={2}>
          <InputField
            className={errors?.categoryName ? "ion-invalid" : "ion-valid"}
            {...register("categoryName", {
              required: "Input ini wajib diisi!",
            })}
            onFocus={(e) => e.currentTarget.showPicker()}
            value={watch('categoryName')}
            onChange={(e) => setValue("categoryName", e.target.value)}
            label="Nama Kategori"
          />
        </Stack>
      </IonContent>
      <IonFooter>
        <IonToolbar>
          <div className="form-button">
            <IonButton size="default" onClick={onCancel} color={`danger`}>
              Cancel
            </IonButton>
            <IonButton
              onClick={handleSubmit(onSubmit, onError)}
              size="default"
              type="button"
            >
              Submit
            </IonButton>
          </div>
        </IonToolbar>
      </IonFooter>
    </IonModal>
  );
};

export default ModalForm;
