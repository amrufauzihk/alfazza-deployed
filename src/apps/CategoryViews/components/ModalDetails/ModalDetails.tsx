import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonModal,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { closeOutline, pencilSharp } from "ionicons/icons";
import React from "react";

type ModalDetailsProps = {
  showDetail: boolean;
  handleClose: () => void;
  handleEdit: () => void;
  data: CategoryProps;
};

const ModalDetails: React.FC<ModalDetailsProps> = ({
  showDetail,
  handleClose,
  handleEdit,
  data,
}) => {
  return (
    <IonModal mode="ios" isOpen={showDetail} onDidDismiss={handleClose}>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Category Detail</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={handleClose}>
              <IonIcon icon={closeOutline} size="large" />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonItem>
          <IonButton
            onClick={handleEdit}
            className="ion-margin"
            color={`success`}
            slot="end"
          >
            <IonIcon icon={pencilSharp} />
            <IonText>Edit</IonText>
          </IonButton>
        </IonItem>
        <IonItem>
          <IonCol>Nama</IonCol>
          <IonCol>{data?.categoryName}</IonCol>
        </IonItem>
      </IonContent>
    </IonModal>
  );
};

export default ModalDetails;
