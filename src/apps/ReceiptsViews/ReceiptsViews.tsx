import {
  IonAlert,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonIcon,
  IonItem,
  IonLoading,
  IonModal,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
  useIonLoading,
  useIonToast,
} from "@ionic/react";
import React, { useEffect, useMemo, useState } from "react";
import { useForm } from "react-hook-form";
import { add, closeOutline, pencilSharp } from "ionicons/icons";
import { MRT_ColumnDef } from "material-react-table";

import {
  createData,
  getAllData,
  multipleDeleteData,
  updateData,
} from "../../firebase/firebaseService";
import MainHeaders from "../../common/MainHeaders";
import Table from "../../common/Table";
import ReceiptsForms from "./ReceiptsForms";

import "./ReceiptsViews.css";

interface Person {
  angsuranKe: string;
  jenisPenerimaan: string;
  keterangan: string;
  namaCustomer: string;
  tglPenerimaan: string;
  totalPenerimaan: string;
}

const ReceiptsViews: React.FC = () => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [showLoading, setShowLoading] = useState(false);
  const [data, setData] = useState(false);
  const [showDetailsModal, setDetailModals] = useState<boolean>(false);
  const [selectedData, setSelectedData] = useState<any>({});
  const [deteleConfirm, setDeleteConfirm] = useState<boolean>(false);
  const [selectedDeletion, setSelectedDeletion] = useState<any[]>([]);

  const {
    register,
    setValue,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm();

  const [showIonLoading, dismissIonLoading] = useIonLoading();
  const [present] = useIonToast();

  const fetchSalesList = async () => {
    try {
      setShowLoading(true);
      const data: any = await getAllData("receipts");
      setShowLoading(false);
      setData(data);
    } catch (error) {
      console.log("error", error);
      present({
        message: "Terjadi Kesalahan, coba reload lagi",
        duration: 1500,
        position: "top",
        color: "danger",
      });
      setShowLoading(false);
    } finally {
      setShowLoading(false);
    }
  };

  useEffect(() => {
    fetchSalesList();
  }, []);

  const columns = useMemo<MRT_ColumnDef<Person>[]>(
    () => [
      {
        accessorKey: "namaCustomer",
        header: "Nama Customer",
        Cell: ({ cell }: any) => {
          return (
            <span
              className="ion-cursor-pointer"
              onClick={() => {
                setSelectedData(cell?.row?.original);
                setDetailModals(true);
              }}
            >
              {cell?.getValue() || "-"}
            </span>
          );
        },
      },
      {
        accessorKey: "jenisPenerimaan",
        header: "Jenis Penerimaan",
        enableHiding: false,
        filterFn: "equals",
        filterSelectOptions: [
          { text: "pembayaran angsuran", value: "pembayaran angsuran" },
          { text: "pendapatan sewa", value: "pendapatan sewa" },
          { text: "pendapatan lainnya", value: "pendapatan lainnya" },
        ],
        filterVariant: "select",
      },
      {
        accessorKey: "keterangan",
        header: "Keterangan",
        enableColumnFilterModes: false,
      },
      {
        accessorKey: "angsuranKe",
        header: "Angsuran Ke",
      },
      {
        accessorKey: "tglPenerimaan",
        header: "Tanggal Penerimaan",
      },
      {
        accessorKey: "totalPenerimaan",
        header: "Total Penerimaan",
      },
    ],
    []
  );

  const handleEdit = () => {
    setModalOpen(true);
    /** SET ALL VALUE !! WE CAN DO IT AUTOMATICALLY, SO THAT SHOULD BE TECHDEBT */
    setValue("angsuranKe", selectedData?.angsuranKe);
    setValue("jenisPenerimaan", selectedData?.jenisPenerimaan);
    setValue("keterangan", selectedData?.keterangan);
    setValue("namaCustomer", selectedData?.namaCustomer);
    setValue("tglPenerimaan", selectedData?.tglPenerimaan);
    setValue("totalPenerimaan", selectedData?.totalPenerimaan);
  };

  const onCancel = () => {
    // something happens here
    setModalOpen(false);
    /** ONLY MOCK: TECHDEBT */
    present({
      message: "Contoh Kalau Gagal menambahkan",
      duration: 1500,
      position: "top",
      color: "danger",
    });
  };

  const onSubmit = async (data: any) => {
    try {
      Object.keys(data).forEach((key) => {
        if (data[key] === undefined) {
          delete data[key];
        }
      });

      showIonLoading("Loading");

      selectedData?.id
        ? await updateData(selectedData.id, data, "receipts")
        : await createData(data, "receipts");

      dismissIonLoading();
      setModalOpen(false);
      present({
        message: "Berhasil ditambahkan",
        duration: 1500,
        position: "top",
        color: "success",
      });
    } catch (error) {
      console.log("error", error);

      present({
        message: "Gagal Menambahkan data, Terjadi Kesalahan",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      handleCloseDetailModal();
      fetchSalesList();
    }
  };

  const handleCloseDetailModal = () => {
    setSelectedData({});
    /** SET ALL VALUE !! WE CAN DO IT AUTOMATICALLY, SO THAT SHOULD BE TECHDEBT */
    setValue("angsuranKe", selectedData?.angsuranKe);
    setValue("jenisPenerimaan", selectedData?.jenisPenerimaan);
    setValue("keterangan", selectedData?.keterangan);
    setValue("namaCustomer", selectedData?.namaCustomer);
    setValue("tglPenerimaan", selectedData?.tglPenerimaan);
    setValue("totalPenerimaan", selectedData?.totalPenerimaan);

    setDetailModals(false);
  };

  const handleDelete = async () => {
    /** HERE IS a FUNCTION TO DELETE THE DATA */
    try {
      showIonLoading("Loading");
      await multipleDeleteData(selectedDeletion, "receipts");
      dismissIonLoading();
      setModalOpen(false);
      present({
        message: "Berhasil menghapus data",
        duration: 1500,
        position: "top",
        color: "success",
      });
    } catch (error) {
      console.log("error", error);
      present({
        message: "Gagal Menghapus data, Terjadi Kesalahan",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      setSelectedDeletion([]);
      setDeleteConfirm(false);
      handleCloseDetailModal();
      fetchSalesList();
    }
  };

  return (
    <IonPage>
      <MainHeaders title="receipts" />
      <IonContent className="ion-padding">
        {/* <Table
          columns={columns}
          data={data}
          showDownload={true}
          handleDeletes={(ids: string[]) => {
            setSelectedDeletion(ids);
            setDeleteConfirm(true);
          }}
        /> */}
        <IonFab
          slot="fixed"
          vertical="bottom"
          horizontal="end"
          className="ion-padding"
          onClick={() => setModalOpen(true)}
        >
          <IonFabButton>
            <IonIcon icon={add}></IonIcon>
          </IonFabButton>
        </IonFab>
        <IonModal mode="ios" isOpen={isModalOpen} onDidDismiss={() => setModalOpen(false)}>
          <IonHeader>
            <IonToolbar>
              <IonTitle>Tambahkan Data</IonTitle>
              <IonButtons slot="end">
                <IonButton onClick={() => setModalOpen(false)}>
                  <IonIcon icon={closeOutline} size="large" />
                </IonButton>
              </IonButtons>
            </IonToolbar>
          </IonHeader>
          <IonContent>
            <ReceiptsForms
              handleSubmit={handleSubmit}
              register={register}
              onCancel={onCancel}
              getValues={getValues}
              onSubmit={onSubmit}
              errors={errors}
            />
          </IonContent>
        </IonModal>
      </IonContent>
      <IonModal mode="ios" isOpen={showDetailsModal} onDidDismiss={handleCloseDetailModal}>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Customer Detail</IonTitle>
            <IonButtons slot="end">
              <IonButton onClick={handleCloseDetailModal}>
                <IonIcon icon={closeOutline} size="large" />
              </IonButton>
            </IonButtons>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonItem>
            <IonButton
              onClick={handleEdit}
              className="ion-margin"
              color={`success`}
              slot="end"
            >
              <IonIcon icon={pencilSharp} />
              <IonText>Edit</IonText>
            </IonButton>
          </IonItem>
          <IonItem>
            <IonCol>Nama Customer</IonCol>
            <IonCol>{selectedData?.namaCustomer}</IonCol>
          </IonItem>
          <IonItem>
            <IonCol>Angsuran Ke</IonCol>
            <IonCol>{selectedData?.angsuranKe}</IonCol>
          </IonItem>
          <IonItem>
            <IonCol>Jenis Penerimaan</IonCol>
            <IonCol>{selectedData?.jenisPenerimaan}</IonCol>
          </IonItem>
          <IonItem>
            <IonCol>Keterangan</IonCol>
            <IonCol>{selectedData?.keterangan}</IonCol>
          </IonItem>
          <IonItem>
            <IonCol>Tanggal Penerimaan</IonCol>
            <IonCol>{selectedData?.tglPenerimaan}</IonCol>
          </IonItem>
          <IonItem>
            <IonCol>Total Penerimaan</IonCol>
            <IonCol>{selectedData?.totalPenerimaan}</IonCol>
          </IonItem>
        </IonContent>
      </IonModal>
      {/* DELETE CONFIRMATION ALERT */}
      <IonAlert
        header="Apakah anda yakin ingin menghapus data ?"
        subHeader="Data yang dihapus tidak dapat dikembalikan lagi"
        isOpen={deteleConfirm}
        buttons={[
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              setDeleteConfirm(false);
              setSelectedDeletion([]);
            },
          },
          {
            text: "OK",
            role: "confirm",
            handler: handleDelete,
          },
        ]}
      ></IonAlert>
      <IonLoading
        cssClass="my-custom-class"
        isOpen={showLoading}
        onDidDismiss={() => setShowLoading(false)}
        message={"Please Wait..."}
      />
    </IonPage>
  );
};

export default ReceiptsViews;
