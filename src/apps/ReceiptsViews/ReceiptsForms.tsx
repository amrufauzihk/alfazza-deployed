import { IonList, IonListHeader, IonItem, IonInput, IonTextarea, IonLabel, IonText, IonItemGroup, IonButton, IonNote, IonSelect, IonSelectOption } from '@ionic/react';
import React, { useState } from 'react';
import Radio from '../../common/Radio';
// import { ErrorMessage } from "@hookform/error-message"

const ReceiptsForms: React.FC<any> = ({
  register,
  handleSubmit,
  onSubmit,
  onCancel,
  errors,
  getValues,
}) => {
  const [selectedPenerimaan, setSelectedPenerimaan] = useState('pembayaran angsuran');

  return (
    <form
      onSubmit={handleSubmit((data: any) => onSubmit(data))}
      onChange={() => setSelectedPenerimaan(getValues()?.jenisPenerimaan || 'pembayaran angsuran')}
    >
      <div className='ion-padding'>
        <div>
          <IonLabel>Jenis Pembelian</IonLabel>
          <div className='ion-margin-top'>
            <Radio
              defaultValue="pembayaran angsuran"
              label="pembayaran angsuran"
              name="jenisPenerimaan"
              register={register}
            />
            <Radio
              defaultValue="pendapatan sewa"
              label="pendapatan sewa"
              name="jenisPenerimaan"
              register={register}
            />
            <Radio
              defaultValue="pendapatan lainnya"
              label="pendapatan lainnya"
              name="jenisPenerimaan"
              register={register}
            />
          </div>
        </div>
        <div>
          <IonInput {...register('totalPenerimaan')} type='number' label="Total Penerimaan" labelPlacement="stacked" placeholder="Enter text"></IonInput>
        </div>
        <div>
          <IonInput {...register('tglPenerimaan')} type='date' label="Tanggal Penerimaan" labelPlacement="stacked" placeholder="Enter text"></IonInput>
        </div>
        {selectedPenerimaan === 'pembayaran angsuran' ? (
          <>
            <div>
              <IonInput {...register('namaCustomer')} type='string' label="Nama Customer" labelPlacement="stacked" placeholder="Enter text"></IonInput>
            </div>
            <div>
              <IonInput {...register('angsuranKe')} type='number' label="Angsuran Ke" labelPlacement="stacked"></IonInput>
            </div>
          </>
        ) : (
          <div>
            <IonTextarea {...register('keterangan')} label="Keterangan" labelPlacement="stacked" placeholder="Enter text"></IonTextarea>
          </div>
        )}
      </div>
      <div className='d-flex ion-justify-content-end ion-padding'>
        <IonButton onClick={onCancel} color={`danger`}>Cancel</IonButton>
        <IonButton type='submit'>Tambahkan Data</IonButton>
      </div>
    </form>
  );
};

export default ReceiptsForms;