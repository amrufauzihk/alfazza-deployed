import {
  IonAlert,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonFab,
  IonFabButton,
  IonFabList,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonLoading,
  IonModal,
  IonPage,
  IonTabBar,
  IonTabButton,
  IonText,
  IonTitle,
  IonToolbar,
  useIonLoading,
  useIonRouter,
  useIonToast,
} from "@ionic/react";
import React, { memo, useCallback, useEffect, useMemo, useState } from "react";
import MainHeaders from "../../common/MainHeaders";
import { MRT_ColumnDef } from "material-react-table";
import Table from "../../common/Table";
import { Box } from "@mui/material";
import {
  add,
  chevronUpCircle,
  closeOutline,
  colorPalette,
  globe,
  library,
  pencilOutline,
  pencilSharp,
  playCircle,
  radio,
  search,
} from "ionicons/icons";
import ExpenseForms from "./ExpenseForms";
import {
  createData,
  getAllData,
  multipleDeleteData,
  updateData,
} from "../../firebase/firebaseService";
import { useForm } from "react-hook-form";

import "./ExpenseViews.css";

interface Person {
  jenisPembayaran: string;
  tanggalPembayaran: string;
  totalPembayaran: string;
  keterangan: string;
}

const ExpenseViews: React.FC = () => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [showLoading, setShowLoading] = useState(false);
  const [data, setData] = useState(false);
  const [showDetailsModal, setDetailModals] = useState<boolean>(false);
  const [selectedData, setSelectedData] = useState<any>({});
  const [deteleConfirm, setDeleteConfirm] = useState<boolean>(false);
  const [selectedDeletion, setSelectedDeletion] = useState<any[]>([]);

  const {
    register,
    setValue,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm();

  const [showIonLoading, dismissIonLoading] = useIonLoading();
  const [present] = useIonToast();

  const fetchSalesList = async () => {
    try {
      setShowLoading(true);
      const data: any = await getAllData("expense");
      setShowLoading(false);
      setData(data);
    } catch (error) {
      console.log("error", error);
      present({
        message: "Terjadi Kesalahan, coba reload lagi",
        duration: 1500,
        position: "top",
        color: "danger",
      });
      setShowLoading(false);
    } finally {
      setShowLoading(false);
    }
  };

  useEffect(() => {
    fetchSalesList();
  }, []);

  const columns = useMemo<MRT_ColumnDef<Person>[]>(
    () => [
      {
        accessorKey: "jenisPembayaran",
        header: "Jenis Pembayaran",
        enableHiding: false,
        Cell: ({ cell, column }: any) => {
          return (
            <span
              className="ion-cursor-pointer"
              onClick={() => {
                setSelectedData(cell?.row?.original);
                setDetailModals(true);
              }}
            >
              {cell?.getValue()}
            </span>
          );
        },
        // accessorFn: (originalRow) => originalRow.age, //alternate way
        // id: 'age', //id required if you use accessorFn instead of accessorKey
        // Cell: ({ cell, column }: any) => (
        //   <span style={{ cursor: 'pointer' }}>{cell?.getValue()}</span>
        // ),
      },
      {
        accessorKey: "tanggalPembayaran",
        header: "Tanggal Pembayaran",
        enableHiding: false,
      },
      {
        accessorKey: "totalPembayaran",
        header: "Total Pembayaran",
        enableColumnFilterModes: false,
      },
      {
        accessorKey: "keterangan",
        header: "Keterangan",
      },
    ],
    []
  );

  const handleEdit = () => {
    setModalOpen(true);
    /** SET ALL VALUE !! WE CAN DO IT AUTOMATICALLY, SO THAT SHOULD BE TECHDEBT */
    setValue("tanggalPembayaran", selectedData?.tanggalPembayaran);
    setValue("jenisPembayaran", selectedData?.jenisPembayaran);
    setValue("totalPembayaran", selectedData?.totalPembayaran);
    setValue("keterangan", selectedData?.keterangan);
  };

  const onCancel = () => {
    // something happens here
    setModalOpen(false);
    /** ONLY MOCK: TECHDEBT */
    present({
      message: "Contoh Kalau Gagal menambahkan",
      duration: 1500,
      position: "top",
      color: "danger",
    });
  };
  const onSubmit = async (data: any) => {
    /** ONLY MOCK: TECHDEBT */
    // console.log('selectedData', selectedData)
    try {
      showIonLoading("Loading");
      selectedData?.id
        ? await updateData(selectedData.id, data, "expense")
        : await createData(data, "expense");

      dismissIonLoading();
      setModalOpen(false);
      present({
        message: "Berhasil ditambahkan",
        duration: 1500,
        position: "top",
        color: "success",
      });
    } catch (error) {
      console.log("error", error);
      present({
        message: "Gagal Menambahkan data, Terjadi Kesalahan",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      handleCloseDetailModal();
      fetchSalesList();
    }
  };

  const handleCloseDetailModal = () => {
    setSelectedData({});
    /** SET ALL VALUE !! WE CAN DO IT AUTOMATICALLY, SO THAT SHOULD BE TECHDEBT */
    setValue("tanggalPembayaran", selectedData?.tanggalPembayaran);
    setValue("jenisPembayaran", selectedData?.jenisPembayaran);
    setValue("totalPembayaran", selectedData?.totalPembayaran);
    setValue("keterangan", selectedData?.keterangan);
    setDetailModals(false);
  };

  const handleDelete = async () => {
    /** HERE IS a FUNCTION TO DELETE THE DATA */
    try {
      showIonLoading("Loading");
      await multipleDeleteData(selectedDeletion, "expense");
      dismissIonLoading();
      setModalOpen(false);
      present({
        message: "Berhasil menghapus data",
        duration: 1500,
        position: "top",
        color: "success",
      });
    } catch (error) {
      console.log("error", error);
      present({
        message: "Gagal Menghapus data, Terjadi Kesalahan",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      setSelectedDeletion([]);
      setDeleteConfirm(false);
      handleCloseDetailModal();
      fetchSalesList();
    }
  };

  return (
    <IonPage>
      <MainHeaders title="expense" />
      <IonContent className="ion-padding">
        {/* <Table
          columns={columns}
          data={data}
          showDownload={true}
          handleDeletes={(ids: string[]) => {
            setSelectedDeletion(ids);
            setDeleteConfirm(true);
          }}
        /> */}
        <IonFab
          slot="fixed"
          vertical="bottom"
          horizontal="end"
          className="ion-padding"
          onClick={() => setModalOpen(true)}
        >
          <IonFabButton>
            <IonIcon icon={add}></IonIcon>
          </IonFabButton>
        </IonFab>
        <IonModal mode="ios" isOpen={isModalOpen} onDidDismiss={() => setModalOpen(false)}>
          <IonHeader>
            <IonToolbar>
              <IonTitle>Tambahkan Data</IonTitle>
              <IonButtons slot="end">
                <IonButton onClick={() => setModalOpen(false)}>
                  <IonIcon icon={closeOutline} size="large" />
                </IonButton>
              </IonButtons>
            </IonToolbar>
          </IonHeader>
          <IonContent>
            <ExpenseForms
              handleSubmit={handleSubmit}
              register={register}
              onCancel={onCancel}
              getValues={getValues}
              onSubmit={onSubmit}
              errors={errors}
            />
          </IonContent>
        </IonModal>
      </IonContent>
      <IonModal mode="ios" isOpen={showDetailsModal} onDidDismiss={handleCloseDetailModal}>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Customer Detail</IonTitle>
            <IonButtons slot="end">
              <IonButton onClick={handleCloseDetailModal}>
                <IonIcon icon={closeOutline} size="large" />
              </IonButton>
            </IonButtons>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonItem>
            <IonButton
              onClick={handleEdit}
              className="ion-margin"
              color={`success`}
              slot="end"
            >
              <IonIcon icon={pencilSharp} />
              <IonText>Edit</IonText>
            </IonButton>
          </IonItem>
          <IonItem>
            <IonCol>Tanggal Pembayaran</IonCol>
            <IonCol>{selectedData?.tanggalPembayaran}</IonCol>
          </IonItem>
          <IonItem color={`light`}>
            <IonCol>Jenis Pembayaran</IonCol>
            <IonCol>{selectedData?.jenisPembayaran}</IonCol>
          </IonItem>
          <IonItem>
            <IonCol>Total Pembayaran</IonCol>
            <IonCol>{selectedData?.totalPembayaran}</IonCol>
          </IonItem>
          <IonItem color={`light`}>
            <IonCol>Keterangan</IonCol>
            <IonCol>{selectedData?.keterangan}</IonCol>
          </IonItem>
        </IonContent>
      </IonModal>
      {/* DELETE CONFIRMATION ALERT */}
      <IonAlert
        header="Apakah anda yakin ingin menghapus data ?"
        subHeader="Data yang dihapus tidak dapat dikembalikan lagi"
        isOpen={deteleConfirm}
        buttons={[
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              setDeleteConfirm(false);
              setSelectedDeletion([]);
            },
          },
          {
            text: "OK",
            role: "confirm",
            handler: handleDelete,
          },
        ]}
      ></IonAlert>
      <IonLoading
        cssClass="my-custom-class"
        isOpen={showLoading}
        onDidDismiss={() => setShowLoading(false)}
        message={"Please Wait..."}
      />
    </IonPage>
  );
};

export default ExpenseViews;
