import { IonList, IonListHeader, IonItem, IonInput, IonTextarea, IonLabel, IonText, IonItemGroup, IonButton, IonNote, IonSelect, IonSelectOption } from '@ionic/react';
import React, { useState } from 'react';
import Radio from '../../common/Radio';
// import { ErrorMessage } from "@hookform/error-message"

const ExpenseForms: React.FC<any> = ({
  register,
  handleSubmit,
  onSubmit,
  onCancel,
  errors,
  getValues,
}) => {
  const [selectedPurchase, setSelectedPurchase] = useState('Inventory');

  return (
    <form
      onSubmit={handleSubmit((data: any) => onSubmit(data))}
      onChange={() => setSelectedPurchase(getValues()?.kindOfTransaction || 'Inventory')}
    >
      <div className='ion-padding'>
        {/* <div className='ion-padding-start ion-padding-top'> */}
        <div>
          <IonInput {...register('tanggalPembayaran')} type='date' label="Tanggal Pembayaran" labelPlacement="stacked"></IonInput>
        </div>
        {/* <div className='ion-padding-start ion-padding-top'> */}
        <div>
          {/* <IonInput {...register('noAkad')} type='text' label="No. Akad" labelPlacement="stacked" placeholder="Enter text"></IonInput> */}
          <IonSelect {...register('jenisPembayaran')} label="Stacked label" labelPlacement="stacked" placeholder="Pilih Jenis Pembayaran">
            <IonSelectOption value="gaji">gaji</IonSelectOption>
            <IonSelectOption value="thr">thr</IonSelectOption>
            <IonSelectOption value="tujangan komunikasi">tujangan komunikasi</IonSelectOption>
            <IonSelectOption value="tunjangan transportasi">tunjangan transportasi</IonSelectOption>
            <IonSelectOption value="biaya meeting">biaya meeting</IonSelectOption>
            <IonSelectOption value="listrik">listrik</IonSelectOption>
            <IonSelectOption value="biaya sewa kantor">biaya sewa kantor</IonSelectOption>
            <IonSelectOption value="administrasi kantor">administrasi kantor</IonSelectOption>
            <IonSelectOption value="administrasi bank">administrasi bank</IonSelectOption>
            <IonSelectOption value="dividen">dividen</IonSelectOption>
            <IonSelectOption value="zakat">zakat</IonSelectOption>
          </IonSelect>
        </div>
        {/* <div className='ion-padding-start ion-padding-top'> */}
        <div>
          <IonInput {...register('totalPembayaran')} type='number' label="Total Pembayaran" labelPlacement="stacked" placeholder="Enter text"></IonInput>
        </div>
        {/* <div className='ion-padding-start ion-padding-top'> */}
        <div>
          <IonTextarea {...register('keterangan')} className='' label="Keterangan" labelPlacement="stacked" placeholder="Enter text"></IonTextarea>
        </div>
      </div>
      <div className='d-flex ion-justify-content-end ion-padding'>
        <IonButton onClick={onCancel} color={`danger`}>Cancel</IonButton>
        <IonButton type='submit'>Tambahkan Data</IonButton>
      </div>
    </form>
  );
};

export default ExpenseForms;