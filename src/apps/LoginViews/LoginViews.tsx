import React from "react";
import { useForm } from "react-hook-form";
import {
  IonButton,
  IonCard,
  IonCardTitle,
  IonContent,
  IonImg,
  IonInput,
  IonPage,
  useIonLoading,
  useIonRouter,
  useIonToast,
} from "@ionic/react";

import { getUserByUsernameAndPassword } from "../../firebase/userService";

import "./LoginViews.css";

const LoginViews: React.FC = () => {
  const router = useIonRouter();
  const [showIonLoading, dismissIonLoading] = useIonLoading();
  const [present] = useIonToast();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const handleLogin = async (data: any) => {
    try {
      showIonLoading("Loading");

      const user = await getUserByUsernameAndPassword(
        data?.username,
        data?.password
      );

      if (user) {
        present({
          message: "Berhasil login",
          duration: 1500,
          position: "top",
          color: "success",
        });
        router.push("/dashboard");
      } else {
        present({
          message: "Username atau password salah",
          duration: 1500,
          position: "top",
          color: "danger",
        });
      }
    } catch (error) {
      console.error("Error saat login:", error);
      present({
        message: "Terjadi kesalahan saat login, ulangi lagi",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      reset();
      dismissIonLoading();
    }
  };

  return (
    <IonPage>
      <IonContent>
        <form
          className="ion-card-wrapper"
          onSubmit={handleSubmit((data: any) => handleLogin(data))}
        >
          <IonCard className="ion-padding">
            <IonImg src="https://alfazzakreditsyariah.netlify.app/img/brand.png" />
            <IonCardTitle
              className="ion
            -margin-top ion-text-center"
            >
              Selamat Datang
            </IonCardTitle>
            <IonCardTitle className="ion-margin-top ion-text-center">
              Alfazza Kredit Syariah
            </IonCardTitle>
            <div>
              <IonInput
                {...register("username", { required: true })}
                label="Username"
                type="text"
                labelPlacement="floating"
                // fill="outline"
                placeholder="Enter text"
                className="ion-margin-top"
                clearInput
                autofocus
              />
              <IonInput
                {...register("password", { required: true })}
                label="Password"
                type="password"
                labelPlacement="floating"
                // fill="outline"
                placeholder="Enter text"
                className="ion-margin-top"
              />
            </div>
            <IonButton
              className="ion-margin-top"
              expand="block"
              type="submit"
              // onClick={handleLogin}
            >
              Login
            </IonButton>
          </IonCard>
        </form>
      </IonContent>
    </IonPage>
  );
};

export default LoginViews;
