import React from "react";
import { closeOutline, pencilSharp } from "ionicons/icons";
import { formatDate } from "../../../../utils/formatDate";
import { formatCurrency } from "../../../../utils/formatNumber";
import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonModal,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react";

type ModalPurchaseDetailsProps = {
  data: PurchaseProps;
  showDetails: boolean;
  handleEdit: (data: PurchaseProps) => void;
  setShowDetails: (key: boolean) => void;
};

const ModalPurchaseDetails: React.FC<ModalPurchaseDetailsProps> = ({
  data,
  handleEdit,
  setShowDetails,
  showDetails,
}) => {
  const handleClose = () => {
    setShowDetails(false);
  };
  return (
    <IonModal mode="ios" isOpen={showDetails} onDidDismiss={handleClose}>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Customer Detail</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={handleClose}>
              <IonIcon icon={closeOutline} size="large" />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonItem>
          <IonButton
            onClick={() => handleEdit(data)}
            className="ion-margin"
            color={`success`}
            slot="end"
          >
            <IonIcon icon={pencilSharp} />
            <IonText>Edit</IonText>
          </IonButton>
        </IonItem>
        <IonItem>
          <IonCol>Jenis Transaksi</IonCol>
          <IonCol>{data?.kindOfTransaction || "-"}</IonCol>
        </IonItem>
        <IonItem color={`light`}>
          <IonCol>Tanggal Transaksi</IonCol>
          <IonCol>
            {data?.tanggalTransaksi ? formatDate(data?.tanggalTransaksi) : "-"}
          </IonCol>
        </IonItem>
        <IonItem>
          <IonCol>Jenis Barang</IonCol>
          <IonCol>{data?.jenisBarang || "-"}</IonCol>
        </IonItem>
        <IonItem color={`light`}>
          <IonCol>Merk</IonCol>
          <IonCol>{data?.merk || "-"}</IonCol>
        </IonItem>
        <IonItem>
          <IonCol>Type</IonCol>
          <IonCol>{data?.type || "-"}</IonCol>
        </IonItem>
        <IonItem color={`light`}>
          <IonCol>Harga Barang</IonCol>
          <IonCol>
            {data?.hargaBarang ? formatCurrency(data?.hargaBarang) : "-"}
          </IonCol>
        </IonItem>
        {data.kindOfTransaction === "Inventory" && (
          <IonItem>
            <IonCol>Potongan</IonCol>
            <IonCol>
              {data?.potongan ? formatCurrency(data?.potongan) : "-"}
            </IonCol>
          </IonItem>
        )}
        {data.kindOfTransaction === "Assets" && (
          <>
            <IonItem color={`light`}>
              <IonCol>Umur Ekonomis</IonCol>
              <IonCol>{data?.umurEkonomis || "-"}</IonCol>
            </IonItem>
            <IonItem>
              <IonCol>Nilai Sisa</IonCol>
              <IonCol>{data?.nilaiSisa || "-"}</IonCol>
            </IonItem>
          </>
        )}
      </IonContent>
    </IonModal>
  );
};

export default ModalPurchaseDetails;
