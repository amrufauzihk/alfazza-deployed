import React from "react";
import { Stack } from "@mui/joy";
import { Calendar } from "lucide-react";
import { closeOutline } from "ionicons/icons";
import {
  CurrencyField,
  DropdownField,
  InputField,
} from "../../../../common/FormField";
import {
  IonButton,
  IonModal,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonButtons,
  IonIcon,
  IonContent,
  IonFooter,
} from "@ionic/react";
import "./PurchaseForm.css";
import {
  UseFormRegister,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import useCategoryList from "../../../../hooks/useCategoryList";

interface PurchaseFormProps {
  isModalOpen: boolean;
  purchaseType: TypeOfTransaction;
  onCancel: () => void;
  register: UseFormRegister<PurchaseProps>;
  watch: UseFormWatch<PurchaseProps>;
  setValue: UseFormSetValue<PurchaseProps>;
  handleSubmit: () => void;
  setModalOpen: (data: boolean) => void;
}

const PurchaseForm: React.FC<PurchaseFormProps> = ({
  purchaseType,
  isModalOpen,
  setModalOpen,
  register,
  watch,
  handleSubmit,
  setValue,
  onCancel,
}) => {
  const { data: listCategory } = useCategoryList();

  return (
    <IonModal
      mode="ios"
      isOpen={isModalOpen}
      onDidDismiss={() => setModalOpen(false)}
    >
      <IonHeader>
        <IonToolbar>
          <IonTitle>
            {!!watch("id") ? "Edit" : "Tambahkan"} {purchaseType}
          </IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={() => setModalOpen(false)}>
              <IonIcon icon={closeOutline} size="large" />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <Stack display="flex" flexDirection="column" gap={2} pb={2}>
          <InputField
            {...register("tanggalTransaksi")}
            value={watch("tanggalTransaksi")}
            required
            type="date"
            onFocus={(e) => e.currentTarget.showPicker()}
            onChange={(e) => setValue("tanggalTransaksi", e.target.value)}
            label="Tanggal Transaksi"
            endDecorator={<Calendar />}
          />
          {/* <DropdownField
            {...register("jenisBarang")}
            required
            label="Jenis Barang"
            placeholder="Select"
            value={watch("jenisBarang")}
            options={listCategory.map((v) => {
              return {
                label: String(v.categoryName),
                value: String(v.categoryName),
              };
            })}
            onChange={(e, v) => {
              e?.preventDefault();
              setValue("jenisBarang", String(v));
            }}
          /> */}
          <InputField
            required
            {...register("jenisBarang")}
            value={watch("jenisBarang")}
            type="text"
            onChange={(e) => setValue("jenisBarang", e.target.value)}
            label="Jenis Barang"
            placeholder="Enter text"
          />
          <InputField
            required
            {...register("merk")}
            value={watch("merk")}
            type="text"
            onChange={(e) => setValue("merk", e.target.value)}
            label="Merk"
            placeholder="Enter text"
          />
          <InputField
            required
            {...register("type")}
            value={watch("type")}
            type="text"
            onChange={(e) => setValue("type", e.target.value)}
            label="Type"
            placeholder="Enter text"
          />
          <CurrencyField
            required
            {...register("hargaBarang")}
            value={watch("hargaBarang")}
            label="Harga Barang"
            onChange={(e) => setValue("hargaBarang", e.target.value)}
            placeholder="Enter text"
          />
          {purchaseType === "Inventory" && (
            <CurrencyField
              required
              {...register("potongan")}
              value={watch("potongan")}
              onChange={(e) => setValue("potongan", e.target.value)}
              label="Potongan"
              placeholder="Enter text"
            />
          )}
          {purchaseType === "Assets" && (
            <>
              <InputField
                required
                {...register("umurEkonomis")}
                value={watch("umurEkonomis")}
                onChange={(e) => setValue("umurEkonomis", e.target.value)}
                type="number"
                label="Umur Ekonomis"
                placeholder="Enter text"
              />
              <CurrencyField
                required
                {...register("nilaiSisa")}
                value={watch("nilaiSisa")}
                onChange={(e) => setValue("nilaiSisa", e.target.value)}
                label="Nilai Sisa"
                placeholder="Enter text"
              />
            </>
          )}
        </Stack>
      </IonContent>
      <IonFooter>
        <IonToolbar>
          <div className="form-button">
            <IonButton size="default" onClick={onCancel} color={`danger`}>
              Cancel
            </IonButton>
            <IonButton onClick={handleSubmit} size="default" type="button">
              Submit
            </IonButton>
          </div>
        </IonToolbar>
      </IonFooter>
    </IonModal>
  );
};

export default PurchaseForm;
