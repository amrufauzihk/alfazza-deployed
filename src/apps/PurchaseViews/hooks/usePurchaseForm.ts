import React, { useEffect, useState } from 'react'
import { INIT_PURCHASE } from '../../../constants/purchase';
import { getAllDataByKindOfTransaction } from '../../../firebase/firebaseService';
import { useIonLoading, useIonToast } from '@ionic/react';
import { SubmitErrorHandler, SubmitHandler, useForm } from 'react-hook-form';
import { createData, multipleDeleteData, updateData } from '../../../firebase/firebaseService';
import { MRT_Row } from 'material-react-table';

const usePurchaseForm = (type: TypeOfTransaction) => {
    const [present] = useIonToast();
    const [showIonLoading, dismissIonLoading] = useIonLoading();
    const [listData, setListData] = useState<PurchaseProps[]>([]);
    const [isLoading, setLoading] = useState<boolean>(false);
    const [showFormModal, setShowFormModal] = useState<boolean>(false);
    const [showDetailModal, setShowDetailModal] = useState<boolean>(false);
    const [selectedDeletion, setSelectedDeletion] = useState<string[]>([]);
    const [showModalConfirmation, setShowModalConfirmation] = useState<boolean>(false);

    const {
        register,
        watch,
        reset: setSelectedData,
        handleSubmit,
        getValues,
        setValue,
        resetField,
        formState: { errors },
    } = useForm<PurchaseProps>({
        defaultValues: { ...INIT_PURCHASE, kindOfTransaction: type }
    });

    const handleCloseModal = (type: 'Form' | 'Detail' | 'Confirmation') => {
        setSelectedData();
        switch (type) {
            case 'Confirmation':
                setShowModalConfirmation(false);
                break;
            case 'Detail':
                setShowDetailModal(false);
                break;
            case 'Form':
                setShowFormModal(false);
                break;

            default:
                break;
        }
    }

    const fetchListPurchase = async () => {
        try {
            setLoading(true);
            const data: PurchaseProps[] = await getAllDataByKindOfTransaction(
                "purchase",
                type
            );
            setListData(data)
            console.log("data", data);
        } catch (error) {
            console.log("error", error);
            present({
                message: "Terjadi Kesalahan, coba reload lagi",
                duration: 1500,
                position: "top",
                color: "danger",
            });
        } finally {
            setLoading(false);
        }
    };

    const onSubmit: SubmitHandler<PurchaseProps> = async (data) => {
        console.log("masuk on Submit", data);

        try {
            showIonLoading('Loading')
            const res = data?.id
                ? await updateData(data.id, data, 'purchase')
                : await createData(data, 'purchase');
            console.log("res create or update", res);
            present({
                message: 'Berhasil ditambahkan',
                duration: 1500,
                position: 'top',
                color: 'success'
            });
            setSelectedData();
        } catch (error) {
            console.log('error', error);
            present({
                message: 'Gagal Menambahkan data, Terjadi Kesalahan',
                duration: 1500,
                position: 'top',
                color: 'danger'
            });
        } finally {
            dismissIonLoading();
            handleCloseModal('Form');
            fetchListPurchase();
        }
    }

    const onError: SubmitErrorHandler<PurchaseProps> = async (data) => {
        console.log("masuk on Error", data);
        present({
            message: 'Gagal Menambahkan data, Terjadi Kesalahan',
            duration: 1500,
            position: 'top',
            color: 'danger'
        });
    }

    const submitForm = handleSubmit(onSubmit, onError);

    const handleDetail = (data: PurchaseProps) => {
        setSelectedData({ ...data, kindOfTransaction: type });
        setShowDetailModal(true);
    }

    const handleEdit = (data: PurchaseProps) => {
        setSelectedData({ ...data, kindOfTransaction: type });
        setShowDetailModal(false);
        setShowFormModal(true);
    }

    const handleConfirmDelete = (rows: MRT_Row<PurchaseProps>[]) => {
        setSelectedDeletion(rows.map((v) => v.original.id || ''));
        setShowModalConfirmation(true);
    }

    const handleDelete = async (rows: MRT_Row<PurchaseProps>[]) => {
        /** HERE IS a FUNCTION TO DELETE THE DATA */
        try {
            showIonLoading("Loading");
            await multipleDeleteData(
                selectedDeletion,
                "purchase"
            );
            dismissIonLoading();
            present({
                message: "Berhasil menghapus data",
                duration: 1500,
                position: "top",
                color: "success",
            });
            setSelectedData();
        } catch (error) {
            console.log("error", error);
            present({
                message: "Gagal Menghapus data, Terjadi Kesalahan",
                duration: 1500,
                position: "top",
                color: "danger",
            });
        } finally {
            setShowModalConfirmation(false);
            fetchListPurchase();
        }
    };

    console.log("getValues", getValues());


    useEffect(() => {
        fetchListPurchase();
        setValue('kindOfTransaction', type);
    }, [type]);

    return {
        errors,
        listData,
        isLoading,
        watch,
        present,
        register,
        getValues,
        resetField,
        submitForm,
        handleEdit,
        handleDelete,
        handleSubmit,
        showIonLoading,
        setSelectedData,
        handleCloseModal,
        dismissIonLoading,
        showDetailModal,
        setShowDetailModal,
        showFormModal,
        setShowFormModal,
        showModalConfirmation,
        setShowModalConfirmation,
        handleConfirmDelete,
        handleDetail,
        setValue
    }
}

export default usePurchaseForm;