import { MRT_ColumnDef } from "material-react-table";
import { formatCurrency } from "../../utils/formatNumber";
import { formatDate } from "../../utils/formatDate";

export const inventoryColumns: MRT_ColumnDef<PurchaseProps>[] = [
  {
    accessorKey: "tanggalTransaksi",
    header: "Tanggal Transaksi",
    Cell: ({ row }) => {
      return (
        <span>
          {formatDate(
            row.original.tanggalTransaksi,
            { dateStyle: "long" },
            "id"
          )}
        </span>
      );
    },
  },
  {
    accessorKey: "jenisBarang",
    header: "Jenis Barang",
    enableHiding: false,
    Cell: ({ row }) => {
      return (
        <span className="ion-cursor-pointer">{row.original.jenisBarang}</span>
      );
    },
  },
  {
    accessorKey: "merk",
    header: "Merk",
  },
  {
    accessorKey: "type",
    header: "Type",
  },
  {
    accessorKey: "hargaBarang",
    header: "Harga Barang",
    Cell: ({ row }) => {
      return <span>{formatCurrency(row.original.hargaBarang)}</span>;
    },
  },
  {
    accessorKey: "potongan",
    header: "Potongan",
    Cell: ({ row }) => {
      return <span>{formatCurrency(row.original.potongan)}</span>;
    },
  },
];

export const suppliesColumns: MRT_ColumnDef<PurchaseProps>[] = [
  {
    accessorKey: "tanggalTransaksi",
    header: "Tanggal Transaksi",
    Cell: ({ row }) => {
      return (
        <span>
          {formatDate(
            row.original.tanggalTransaksi,
            { dateStyle: "long" },
            "id"
          )}
        </span>
      );
    },
  },
  {
    accessorKey: "jenisBarang",
    header: "Jenis Barang",
    enableHiding: false,
    Cell: ({ row }) => {
      return (
        <span className="ion-cursor-pointer">{row.original.jenisBarang}</span>
      );
    },
  },
  {
    accessorKey: "merk",
    header: "Merk",
  },
  {
    accessorKey: "type",
    header: "Type",
  },
  {
    accessorKey: "hargaBarang",
    header: "Harga Barang",
    Cell: ({ row }) => {
      return <span>{formatCurrency(row.original.hargaBarang)}</span>;
    },
  },
];

export const assetsColumns: MRT_ColumnDef<PurchaseProps>[] = [
  {
    accessorKey: "tanggalTransaksi",
    header: "Tanggal Transaksi",
    Cell: ({ row }) => {
      return (
        <span>
          {formatDate(
            row.original.tanggalTransaksi,
            { dateStyle: "long" },
            "id"
          )}
        </span>
      );
    },
  },
  {
    accessorKey: "jenisBarang",
    header: "Jenis Barang",
    enableHiding: false,
    Cell: ({ row }) => {
      return (
        <span className="ion-cursor-pointer">{row.original.jenisBarang}</span>
      );
    },
  },
  {
    accessorKey: "merk",
    header: "Merk",
  },
  {
    accessorKey: "type",
    header: "Type",
  },
  {
    accessorKey: "hargaBarang",
    header: "Harga Barang",
    Cell: ({ row }) => {
      return <span>{formatCurrency(row.original.hargaBarang)}</span>;
    },
  },
  {
    accessorKey: "umurEkonomis",
    header: "Umur Ekonomis",
    Cell: ({ row }) => {
      return <span>{row.original.umurEkonomis}</span>;
    },
  },
  {
    accessorKey: "nilaiSisa",
    header: "Nilai Sisa",
    Cell: ({ row }) => {
      return <span>{formatCurrency(row.original.nilaiSisa)}</span>;
    },
  },
];

export const tabsValue = ["Inventory", "Supplies", "Assets"];
