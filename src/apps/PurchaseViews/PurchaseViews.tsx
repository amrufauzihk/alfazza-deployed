import React, { useState } from "react";
import MainHeaders from "../../common/MainHeaders";
import { IonAlert, IonContent, IonPage, useIonRouter } from "@ionic/react";
import { Tab, tabClasses, TabList, TabPanel, Tabs } from "@mui/joy";
import PageHeader from "../../common/PageHeader";
import InventoryViews from "./views/InventoryViews";
import ModalPurchaseDetails from "./components/ModalDetails/ModalDetails";
import usePurchaseForm from "./hooks/usePurchaseForm";
import PurchaseForm from "./components/PurchaseForm/PurchaseForm";
import { tabsValue } from "./constants";
import SuppliesViews from "./views/SuppliesViews";
import AssetsViews from "./views/AssetsViews";

const PurchaseViews: React.FC = () => {
  const [purchaseType, setPurchaseType] =
    useState<TypeOfTransaction>("Inventory");

  const {
    errors,
    listData,
    isLoading,
    watch,
    present,
    register,
    setValue,
    getValues,
    resetField,
    submitForm,
    handleEdit,
    handleDelete,
    handleSubmit,
    showIonLoading,
    setSelectedData,
    handleCloseModal,
    dismissIonLoading,
    showDetailModal,
    setShowDetailModal,
    showFormModal,
    setShowFormModal,
    showModalConfirmation,
    setShowModalConfirmation,
    handleConfirmDelete,
    handleDetail,
  } = usePurchaseForm(purchaseType);

  console.log("listData", listData);

  return (
    <IonPage>
      <MainHeaders title="Purchase Supplies" />
      <IonContent className="ion-padding">
        <PageHeader
          title="Purchase"
          description="Connect purchases to inventory instantly for Finance Team, ensuring smooth integration and automatic updates to the inventory list."
        />
        <Tabs
          aria-label="Basic tabs"
          defaultValue={0}
          onChange={(_, v) => {
            setPurchaseType(tabsValue[v as number] as TypeOfTransaction);
            setSelectedData();
          }}
          sx={{ bgcolor: "transparent" }}
          variant="solid"
          color="success"
        >
          <TabList
            disableUnderline
            sx={{
              p: 0.5,
              gap: 0.5,
              borderRadius: "xl",
              bgcolor: "background.level1",
              [`& .${tabClasses.root}[aria-selected="true"]`]: {
                boxShadow: "sm",
                bgcolor: "background.surface",
              }
            }}
          >
            <Tab className='purchase-tabs'>Inventory</Tab>
            <Tab className='purchase-tabs'>Supplies</Tab>
            <Tab className='purchase-tabs'>Assets</Tab>
          </TabList>
          <TabPanel value={0} sx={{ backgroundColor: "white" }}>
            <InventoryViews
              listData={listData}
              handleDetail={handleDetail}
              setShowFormModal={setShowFormModal}
              handleConfirmDelete={handleConfirmDelete}
            />
          </TabPanel>
          <TabPanel value={1}>
            <SuppliesViews
              listData={listData}
              handleDetail={handleDetail}
              setShowFormModal={setShowFormModal}
              handleConfirmDelete={handleConfirmDelete}
            />
          </TabPanel>
          <TabPanel value={2}>
            <AssetsViews
              listData={listData}
              handleDetail={handleDetail}
              setShowFormModal={setShowFormModal}
              handleConfirmDelete={handleConfirmDelete}
            />
          </TabPanel>
        </Tabs>
      </IonContent>
      {/* Modals */}
      <ModalPurchaseDetails
        data={getValues()}
        showDetails={showDetailModal}
        setShowDetails={setShowDetailModal}
        handleEdit={handleEdit}
      />
      <PurchaseForm
        handleSubmit={submitForm}
        isModalOpen={showFormModal}
        setModalOpen={setShowFormModal}
        onCancel={() => handleCloseModal("Form")}
        purchaseType={purchaseType}
        register={register}
        setValue={setValue}
        watch={watch}
        key={`Form-Purchase`}
      />

      {/* Alerts */}
      <IonAlert
        header="Apakah anda yakin ingin menghapus data ?"
        subHeader="Data yang dihapus tidak dapat dikembalikan lagi"
        isOpen={showModalConfirmation}
        buttons={[
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              setShowModalConfirmation(false);
            },
          },
          {
            text: "OK",
            role: "confirm",
            handler: handleDelete,
          },
        ]}
      ></IonAlert>
    </IonPage>
  );
};

export default PurchaseViews;
