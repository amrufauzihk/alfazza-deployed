import React from "react";
import { MRT_Row } from "material-react-table";
import Table from "../../../../common/Table";

import "./AssetsViews.css";
import { Button, Grid, Stack } from "@mui/joy";
import { Trash2 } from "lucide-react";
import { assetsColumns, inventoryColumns } from "../../constants";

//simple data example - Check out https://www.material-react-table.com/docs/examples/remote for a more complex example
//If using TypeScript, define the shape of your data (optional, but recommended)

interface AssetsViewsProps {
  listData: PurchaseProps[];
  handleDetail: (data: PurchaseProps) => void;
  setShowFormModal: (data: boolean) => void;
  handleConfirmDelete: (rows: MRT_Row<PurchaseProps>[]) => void;
}

const AssetsViews: React.FC<AssetsViewsProps> = ({
  listData,
  handleDetail,
  handleConfirmDelete,
  setShowFormModal,
}) => {
  return (
    <Stack>
      <Table<PurchaseProps>
        section="Purchase Assets"
        columns={assetsColumns}
        data={listData}
        onInsert={() => setShowFormModal(true)}
        enableRowSelection
        onBodyRowClick={({ row }) => ({
          onClick: (event) => {
            event.preventDefault();
            handleDetail(row?.original);
          },
          sx: { cursor: "pointer" },
        })}
        renderCustomActions={({ row }) => {
          return (
            <Button
              color="danger"
              variant="plain"
              onClick={(e) => {
                e.stopPropagation();
                handleConfirmDelete([row]);
              }}
            >
              <Trash2 />
            </Button>
          );
        }}
        renderBulkActions={({ getSelectedRowModel }) => {
          const { rows } = getSelectedRowModel();
          return (
            <Grid container gap={1}>
              <Button
                variant="outlined"
                color="danger"
                onClick={() => handleConfirmDelete(rows)}
                startDecorator={<Trash2 />}
              >
                Delete
              </Button>
            </Grid>
          );
        }}
      />
    </Stack>
  );
};

export default AssetsViews;
