import { IonButtons, IonContent, IonHeader, IonItem, IonLabel, IonMenuButton, IonPage, IonTitle, IonToolbar, useIonRouter } from '@ionic/react';
import React from 'react';
import MainHeaders from '../../common/MainHeaders';
import UnderConstruction from '../../common/UnderConstruction';

const DashboardViews: React.FC = () => {
  // const router = useIonRouter();
  // console.log('router1', router.routeInfo.pathname)
  // const name = router.routeInfo.pathname.replace('/', '');

  return (
    <IonPage>
      {/* <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>{capitalizeFirstLetter(name)}</IonTitle>
        </IonToolbar>
      </IonHeader> */}
      <MainHeaders title='Dashboard' />
      <IonContent>
        <UnderConstruction />
      </IonContent>
    </IonPage>
  );
};

export default DashboardViews;