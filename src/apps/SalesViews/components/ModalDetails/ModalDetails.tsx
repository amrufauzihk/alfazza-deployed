import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonModal,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { closeOutline, pencilSharp } from "ionicons/icons";
import React from "react";

type ModalDetailsProps = {
  showDetail: boolean;
  handleClose: () => void;
  handleEdit: () => void;
  data: SalesProps;
};

const ModalDetails: React.FC<ModalDetailsProps> = ({
  showDetail,
  handleClose,
  handleEdit,
  data,
}) => {
  return (
    <IonModal mode="ios" isOpen={showDetail} onDidDismiss={handleClose}>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Customer Detail</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={handleClose}>
              <IonIcon icon={closeOutline} size="large" />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonItem>
          <IonButton
            onClick={handleEdit}
            className="ion-margin"
            color={`success`}
            slot="end"
          >
            <IonIcon icon={pencilSharp} />
            <IonText>Edit</IonText>
          </IonButton>
        </IonItem>
        <IonItem>
          <IonCol>Nama</IonCol>
          <IonCol>{data?.namaCustomer}</IonCol>
        </IonItem>
        <IonItem color={`light`}>
          <IonCol>Nama Barang</IonCol>
          <IonCol>{data?.namaBarang}</IonCol>
        </IonItem>
        <IonItem>
          <IonCol>No Hp</IonCol>
          <IonCol>{data?.noHp}</IonCol>
        </IonItem>
        <IonItem color={`light`}>
          <IonCol>No Akad</IonCol>
          <IonCol>{data?.noAkad}</IonCol>
        </IonItem>
        <IonItem>
          <IonCol>Angsuran Per Bulan</IonCol>
          <IonCol>{data?.angsuranPerBulan}</IonCol>
        </IonItem>
        <IonItem color={`light`}>
          <IonCol>DP</IonCol>
          <IonCol>{data?.dp}</IonCol>
        </IonItem>
        <IonItem>
          <IonCol>Alamat</IonCol>
          <IonCol>{data?.alamat}</IonCol>
        </IonItem>
        <IonItem color={`light`}>
          <IonCol>Tanggal Transaksi</IonCol>
          <IonCol>{data?.tanggalTransaksi}</IonCol>
        </IonItem>
        <IonItem>
          <IonCol>Referal</IonCol>
          <IonCol>{data?.referal}</IonCol>
        </IonItem>
        <IonItem color={`light`}>
          <IonCol>Tenor</IonCol>
          <IonCol>{data?.tenor}</IonCol>
        </IonItem>
      </IonContent>
    </IonModal>
  );
};

export default ModalDetails;
