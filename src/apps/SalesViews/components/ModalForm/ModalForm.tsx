import {
  IonButton,
  IonButtons,
  IonContent,
  IonFooter,
  IonHeader,
  IonIcon,
  IonModal,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import { closeOutline, watch } from "ionicons/icons";
import {
  FieldErrors,
  UseFormHandleSubmit,
  UseFormRegister,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import { Stack } from "@mui/joy";
import {
  CurrencyField,
  InputField,
  TextAreaField,
} from "../../../../common/FormField";

type ModalFormProps = {
  showModal: boolean;
  setShowModal: (e: boolean) => void;
  handleSubmit: UseFormHandleSubmit<SalesProps, undefined>;
  register: UseFormRegister<SalesProps>;
  onCancel: () => void;
  onSubmit: (data: any) => Promise<void>;
  onError: (data: any) => Promise<void>;
  watch: UseFormWatch<SalesProps>
  setValue: UseFormSetValue<SalesProps>;
  errors: FieldErrors<SalesProps>;
};

const ModalForm: React.FC<ModalFormProps> = ({
  showModal,
  setShowModal,
  handleSubmit,
  register,
  onCancel,
  onSubmit,
  onError,
  watch,
  setValue,
  errors,
}) => {
  return (
    <IonModal mode="ios" isOpen={showModal} onDidDismiss={() => setShowModal(false)}>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tambahkan Data</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={() => setShowModal(false)}>
              <IonIcon icon={closeOutline} size="large" />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <Stack display="flex" flexDirection="column" gap={2} p={2}>
          <InputField
            className={errors?.tanggalTransaksi ? "ion-invalid" : "ion-valid"}
            {...register("tanggalTransaksi", {
              required: "Input ini wajib diisi!",
            })}
            onFocus={(e) => e.currentTarget.showPicker()}
            type="date"
            value={watch('tanggalTransaksi')}
            onChange={(e) => setValue("tanggalTransaksi", e.target.value)}
            label="Tanggal Transaksi"
          />
          <InputField
            className={errors?.noAkad ? "ion-invalid" : "ion-valid"}
            {...register("noAkad", { required: "Input ini wajib diisi!" })}
            type="text"
            onChange={(e) => setValue("noAkad", e.target.value)}
            value={watch('noAkad')}
            label="No. Akad"
            placeholder="Enter text"
          />
          <InputField
            className={errors?.namaCustomer ? "ion-invalid" : "ion-valid"}
            {...register("namaCustomer", {
              required: "Input ini wajib diisi!",
            })}
            type="text"
            onChange={(e) => setValue("namaCustomer", e.target.value)}
            value={watch('namaCustomer')}
            label="Nama Customer"
            placeholder="Enter text"
          />
          <InputField
            className={errors?.namaBarang ? "ion-invalid" : "ion-valid"}
            {...register("namaBarang", { required: "Input ini wajib diisi!" })}
            type="text"
            onChange={(e) => setValue("namaBarang", e.target.value)}
            value={watch('namaBarang')}
            label="Nama Barang"
            placeholder="Enter text"
          />
          <TextAreaField
            className={errors?.namaBarang ? "ion-invalid" : "ion-valid"}
            {...register("alamat")}
            value={watch('alamat')}
            label="Alamat"
            placeholder="Enter text"
            onChange={(e) => setValue('alamat', e.target.value)}
          />
          <InputField
            className={errors?.noHp ? "ion-invalid" : "ion-valid"}
            {...register("noHp", { required: "Input ini wajib diisi!" })}
            type="text"
            value={watch('noHp')}
            onChange={(e) => setValue("noHp", e.target.value)}
            label="No. Hp"
            placeholder="Enter text"
          />
          <InputField
            className={errors?.referal ? "ion-invalid" : "ion-valid"}
            {...register("referal", { required: "Input ini wajib diisi!" })}
            type="text"
            value={watch('referal')}
            onChange={(e) => setValue("referal", e.target.value)}
            label="Referal / PIC"
            placeholder="Enter text"
          />
          <CurrencyField
            className={errors?.dp ? "ion-invalid" : "ion-valid"}
            {...register("dp", { required: "Input ini wajib diisi!" })}
            value={watch('dp')}
            onChange={(e) => setValue("dp", e.target.value)}
            label="DP / Uang Muka"
            placeholder="Enter text"
          />
          <CurrencyField
            className={errors?.tenor ? "ion-invalid" : "ion-valid"}
            {...register("tenor", { required: "Input ini wajib diisi!" })}
            value={watch('tenor')}
            onChange={(e) => setValue("tenor", e.target.value)}
            label="Tenor"
            placeholder="Enter text"
          />
          <CurrencyField
            className={errors?.angsuranPerBulan ? "ion-invalid" : "ion-valid"}
            {...register("angsuranPerBulan", {
              required: "Input ini wajib diisi!",
            })}
            value={watch('angsuranPerBulan')}
            type="text"
            onChange={(e) => setValue("angsuranPerBulan", e.target.value)}
            label="Angsuran Per Bulan"
            placeholder="Enter text"
          />
        </Stack>
      </IonContent>
      <IonFooter>
        <IonToolbar>
          <div className="form-button">
            <IonButton size="default" onClick={onCancel} color={`danger`}>
              Cancel
            </IonButton>
            <IonButton
              onClick={handleSubmit(onSubmit, onError)}
              size="default"
              type="button"
            >
              Submit
            </IonButton>
          </div>
        </IonToolbar>
      </IonFooter>
    </IonModal>
  );
};

export default ModalForm;
