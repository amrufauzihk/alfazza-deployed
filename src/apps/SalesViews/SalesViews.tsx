import React, { useEffect, useMemo, useState } from "react";
import { useForm } from "react-hook-form";
import { MRT_ColumnDef, MRT_Row } from "material-react-table";
import Table from "../../common/Table";
import MainHeaders from "../../common/MainHeaders";
import {
  IonAlert,
  IonContent,
  IonLoading,
  IonPage,
  useIonLoading,
  useIonToast,
} from "@ionic/react";
import {
  createData,
  getAllData,
  multipleDeleteData,
  updateData,
} from "../../firebase/firebaseService";
import { Button, Grid } from "@mui/joy";
import { Trash2 } from "lucide-react";
import PageHeader from "../../common/PageHeader";
import { formatCurrency } from "../../utils/formatNumber";
import { formatDate } from "../../utils/formatDate";
import ModalDetails from "./components/ModalDetails/ModalDetails";
import ModalForm from "./components/ModalForm/ModalForm";
import { INIT_SALES_DATA } from "./constants";

const SalesViews: React.FC = () => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [showLoading, setShowLoading] = useState(false);
  const [data, setData] = useState<SalesProps[]>([]);
  const [showDetailsModal, setDetailModals] = useState<boolean>(false);
  const [selectedData, setSelectedData] = useState<SalesProps>(INIT_SALES_DATA);
  const [deteleConfirm, setDeleteConfirm] = useState<boolean>(false);
  const [selectedDeletion, setSelectedDeletion] = useState<any[]>([]);

  const {
    register,
    setValue,
    reset,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm<SalesProps>({ defaultValues: INIT_SALES_DATA });

  const [showIonLoading, dismissIonLoading] = useIonLoading();
  const [present] = useIonToast();
  console.log("watch", watch('alamat'));
  
  const fetchSalesList = async () => {
    try {
      setShowLoading(true);
      const data: any = await getAllData("sales");
      setShowLoading(false);
      setData(data);
    } catch (error) {
      console.log("error", error);
      present({
        message: "Terjadi Kesalahan, coba reload lagi",
        duration: 1500,
        position: "top",
        color: "danger",
      });
      setShowLoading(false);
    } finally {
      setShowLoading(false);
    }
  };

  useEffect(() => {
    fetchSalesList();
  }, []);

  const columns = useMemo<MRT_ColumnDef<SalesProps>[]>(
    () => [
      {
        accessorKey: "noAkad",
        header: "No Akad",
      },
      {
        accessorKey: "tanggalTransaksi",
        header: "Tanggal Transaksi",
        Cell: ({ row }) => {
          return (
            <span className="ion-cursor-pointer">
              {formatDate(
                row.original.tanggalTransaksi,
                { dateStyle: "long" },
                "id"
              )}
            </span>
          );
        },
      },
      {
        accessorKey: "namaCustomer",
        header: "Nama",
      },
      {
        accessorKey: "namaBarang",
        header: "Nama Barang",
      },
      {
        accessorKey: "noHp",
        header: "No Hp",
        enableColumnFilterModes: false,
      },
      {
        accessorKey: "angsuranPerBulan",
        header: "Angsuran Per Bulan",
        Cell: ({ row }) => {
          return (
            <span className="ion-cursor-pointer">
              {formatCurrency(row.original.angsuranPerBulan)}
            </span>
          );
        },
      },
      {
        accessorKey: "dp",
        header: "DP",
        Cell: ({ row }) => {
          return (
            <span className="ion-cursor-pointer">
              {formatCurrency(row.original.dp)}
            </span>
          );
        },
      },
      {
        accessorKey: "alamat",
        header: "Alamat",
      },
      {
        accessorKey: "referal",
        header: "Referal",
      },
      {
        accessorKey: "tenor",
        header: "Tenor",
        Cell: ({ row }) => {
          return (
            <span className="ion-cursor-pointer">
              {formatCurrency(row.original.tenor)}
            </span>
          );
        },
        enableColumnFilter: false,
      },
    ],
    []
  );

  const handleEdit = () => {
    setModalOpen(true);
    /** SET ALL VALUE !! WE CAN DO IT AUTOMATICALLY, SO THAT SHOULD BE TECHDEBT */
    reset(selectedData);
  };

  const onCancel = () => {
    // something happens here
    setModalOpen(false);
    /** ONLY MOCK: TECHDEBT */
    present({
      message: "Contoh Kalau Gagal menambahkan",
      duration: 1500,
      position: "top",
      color: "danger",
    });
  };

  const onSubmit = async (data: any) => {
    /** ONLY MOCK: TECHDEBT */
    console.log("onSubmit", data);
    
    try {
      showIonLoading("Loading");
      selectedData?.id
        ? await updateData(selectedData.id, data, "sales")
        : await createData(data, "sales");
      setModalOpen(false);
      present({
        message: "Berhasil ditambahkan",
        duration: 1500,
        position: "top",
        color: "success",
      });
    } catch (error) {
      console.log("error", error);
      present({
        message: "Gagal Menambahkan data, Terjadi Kesalahan",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      dismissIonLoading();
      handleCloseDetailModal();
      fetchSalesList();
    }
  };

  const onError = async (data: any) => {
    console.log("error form", data);
    present({
      message: "Lengkapi Semua Data",
      duration: 1500,
      position: "bottom",
      color: "warning",
    });
  }

  const handleCloseDetailModal = () => {
    /** SET ALL VALUE !! WE CAN DO IT AUTOMATICALLY, SO THAT SHOULD BE TECHDEBT */
    setSelectedData(INIT_SALES_DATA)
    reset(INIT_SALES_DATA);
    setDetailModals(false);
  };

  const handleDelete = async (rows: MRT_Row<SalesProps>[]) => {
    console.log("selectedDeletion", selectedDeletion);
    /** HERE IS a FUNCTION TO DELETE THE DATA */
    try {
      showIonLoading("Loading");
      await multipleDeleteData(
        rows.map((v) => {
          return v.original.id || "";
        }),
        "sales"
      );
      dismissIonLoading();
      setModalOpen(false);
      present({
        message: "Berhasil menghapus data",
        duration: 1500,
        position: "top",
        color: "success",
      });
    } catch (error) {
      console.log("error", error);
      present({
        message: "Gagal Menghapus data, Terjadi Kesalahan",
        duration: 1500,
        position: "top",
        color: "danger",
      });
    } finally {
      setSelectedDeletion([]);
      setDeleteConfirm(false);
      handleCloseDetailModal();
      fetchSalesList();
    }
  };
  console.log("selectedData", selectedData);
  

  return (
    <IonPage>
      <MainHeaders title="Sales" />
      <IonContent className="ion-padding">
        <IonContent className="ion-padding">
          <PageHeader
            title="Sales"
            description="Effortlessly input credit sales with integrated data, clear customer ID, easy navigation, referrals, and product integration."
          />
          <Table<SalesProps>
            section="Sales"
            columns={columns}
            data={data}
            onInsert={() => setModalOpen(true)}
            enableRowSelection
            onBodyRowClick={({ row }) => ({
              onClick: (event) => {
                event.preventDefault();
                setSelectedData(row?.original);
                setDetailModals(true);
              },
              sx: { cursor: "pointer" },
            })}
            renderCustomActions={({ row }) => {
              return (
                <Button
                  color="danger"
                  variant="plain"
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelectedDeletion([row.original.id]);
                    setDeleteConfirm(true);
                  }}
                >
                  <Trash2 />
                </Button>
              );
            }}
            renderBulkActions={({ getSelectedRowModel }) => {
              const { rows } = getSelectedRowModel();
              return (
                <Grid container gap={1}>
                  <Button
                    variant="outlined"
                    color="danger"
                    onClick={() => handleDelete(rows)}
                    startDecorator={<Trash2 />}
                  >
                    Delete
                  </Button>
                </Grid>
              );
            }}
          />
        </IonContent>
      </IonContent>
      <ModalForm
        errors={errors}
        handleSubmit={handleSubmit}
        onCancel={onCancel}
        onSubmit={onSubmit}
        onError={onError}
        setValue={setValue}
        watch={watch}
        register={register}
        setShowModal={setModalOpen}
        showModal={isModalOpen}
      />
      <ModalDetails
        data={selectedData}
        handleClose={handleCloseDetailModal}
        handleEdit={handleEdit}
        showDetail={showDetailsModal}
      />
      {/* DELETE CONFIRMATION ALERT */}
      <IonAlert
        header="Apakah anda yakin ingin menghapus data ?"
        subHeader="Data yang dihapus tidak dapat dikembalikan lagi"
        isOpen={deteleConfirm}
        buttons={[
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              setDeleteConfirm(false);
              setSelectedDeletion([]);
            },
          },
          {
            text: "OK",
            role: "confirm",
            handler: handleDelete,
          },
        ]}
      ></IonAlert>
      <IonLoading
        cssClass="my-custom-class"
        isOpen={showLoading}
        onDidDismiss={() => setShowLoading(false)}
        message={"Please Wait..."}
      />
    </IonPage>
  );
};

export default SalesViews;
