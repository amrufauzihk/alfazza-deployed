import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyDNO89th29UzSiKLhc-ey-B2ESKIMAlvrQ",
  authDomain: "alfazza-cloud-system.firebaseapp.com",
  projectId: "alfazza-cloud-system",
  storageBucket: "alfazza-cloud-system.appspot.com",
  messagingSenderId: "353340913502",
  appId: "1:353340913502:web:a46e955329b890f44c41cf",
  measurementId: "G-37YVFGFE4J"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const firestore = firebase.firestore();

export default firestore;
