import firebase from "firebase/compat";
import firestore from "./firebase";
import "firebase/compat/firestore";

export function getAllData(collectionName: string) {
  return firestore.collection(collectionName).get()
    .then((snapshot) => {
      const data = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
      return data;
    })
    .catch((error) => {
      console.error("Error saat mengambil data:", error);
      throw error;
    });
}

export function getAllDataByKindOfTransaction(collectionName: string, kindOfTransaction: TypeOfTransaction) {
  return firestore.collection(collectionName).where('kindOfTransaction', '==', kindOfTransaction).get()
    .then((snapshot) => {
      const data = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
      console.log("data from firebase", data);

      return data as PurchaseProps[];
    })
    .catch((error) => {
      console.error("Error saat mengambil data:", error);
      throw error;
    });
}

export function getDataById(id: string | undefined, collectionName: string) {
  return firestore.collection(collectionName).doc(id).get()
    .then((doc) => {
      if (doc.exists) {
        return { id: doc.id, ...doc.data() };
      } else {
        return null;
      }
    })
    .catch((error) => {
      throw error;
    });
}

export function createData(data: firebase.firestore.DocumentData, collectionName: string) {
  return firestore.collection(collectionName).add(data)
    .then((docRef) => {
      console.log("docRef", docRef);
      return 'success';
    })
    .catch((error) => {
      throw error;
    });
}

export function updateData(id: string | undefined, newData: firebase.firestore.UpdateData, collectionName: string) {
  return firestore.collection(collectionName).doc(id).update(newData)
    .then(res => {
      console.log("res", res);
      return 'success';
    })
    .catch((error) => {
      throw error;
    });
}

export function deleteData(id: string | undefined, collectionName: string) {
  return firestore.collection(collectionName).doc(id).delete()
    .then(() => {
      console.log("Data berhasil dihapus");
    })
    .catch((error) => {
      console.error("Error saat menghapus data:", error);
      throw error;
    });
}

export function multipleDeleteData(ids: string[] | undefined, collectionName: string) {
  if (!ids || ids.length === 0) {
    // Handle case when no IDs are provided
    console.warn("No IDs provided for deletion.");
    return Promise.resolve();
  }

  const deletionPromises: Promise<void>[] = [];

  for (const id of ids) {
    const deletePromise = firestore.collection(collectionName).doc(id).delete();
    deletionPromises.push(deletePromise);
  }

  return Promise.all(deletionPromises)
    .then(() => {
      console.log("Data berhasil dihapus");
    })
    .catch((error) => {
      console.error("Error saat menghapus data:", error);
      throw error;
    });
}

// export function getDataByEventName(eventName) {
//   return firestore.collection(collectionName)
//     .where('eventName', '===', eventName)
//     .get()
//     .then((querySnapshot) => {
//       const data = [];
//       querySnapshot.forEach((doc) => {
//         data.push({ id: doc.id, ...doc.data() });
//       });
//       return data;
//     })
//     .catch((error) => {
//       console.error("Error saat mengambil data:", error);
//       throw error;
//     });
// }

export function getDataByEventName(eventName: string, collectionName: string) {
  const eventNameLowerCase = eventName.toLowerCase();
  return firestore.collection(collectionName)
    .get()
    .then((querySnapshot) => {
      const data: any[] | PromiseLike<any[]> = [];
      querySnapshot.forEach((doc) => {
        const eventData = doc.data();
        const lowercaseEventName = eventData.eventName.toLowerCase();
        if (lowercaseEventName.includes(eventNameLowerCase)) {
          data.push({ id: doc.id, ...eventData });
        }
      });
      return data;
    })
    .catch((error) => {
      console.error("Error saat mengambil data:", error);
      throw error;
    });
}
