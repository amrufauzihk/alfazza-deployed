// ./firebase/userService.ts
import firestore from "./firebase";
import "firebase/compat/firestore";

const collectionName = 'users';

interface User {
  // Define the user properties here based on the actual user data structure
  // For example: id: string; name: string; email: string; ...
}

export async function getUserByUsernameAndPassword(username: string, password: string): Promise<User | null> {
  try {
    const querySnapshot = await firestore
      .collection(collectionName)
      .where('username', '==', username)
      .where('password', '==', password)
      .limit(1)
      .get();

    if (!querySnapshot.empty) {
      const doc = querySnapshot.docs[0];
      return { id: doc.id, ...doc.data() } as User;
    }

    return null;
  } catch (error) {
    console.error("Error saat mengambil data user:", error);
    throw error;
  }
}
