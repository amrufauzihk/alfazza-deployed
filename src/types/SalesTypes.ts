interface SalesProps {
    id?: string;
    name: string;
    age: number;
    noHp: string;
    noAkad: string;
    angsuranPerBulan: string;
    dp: string;
    namaBarang: string;
    namaCustomer: string;
    alamat: string;
    tanggalTransaksi: string;
    referal: string;
    tenor: string;
}