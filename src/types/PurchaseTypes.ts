interface PurchaseProps {
    id?: string;
    hargaBarang: string;
    jenisBarang: string;
    merk: string;
    tanggalTransaksi: string;
    type: string;
    kindOfTransaction: string;
    potongan?: string;
    umurEkonomis?: string;
    nilaiSisa?: string;
}

type TypeOfTransaction = "Inventory" | "Assets" | "Supplies";