import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React, { memo } from 'react';
import SalesViews from '../apps/SalesViews';

const Sales: React.FC = () => {
  return <SalesViews />
};

export default memo(Sales);
