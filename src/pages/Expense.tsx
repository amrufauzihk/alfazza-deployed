import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';
import ExpenseViews from '../apps/ExpenseViews';

const Expense: React.FC = () => {

  return <ExpenseViews />
};

export default Expense;