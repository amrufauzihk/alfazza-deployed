import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';
import LoginViews from '../apps/LoginViews';

const Login: React.FC = () => {

  return (
    <LoginViews />
  );
};

export default Login;