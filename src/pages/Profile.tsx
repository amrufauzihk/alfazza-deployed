import React from 'react';
import ProfileViews from '../apps/ProfileViews';

const Profile: React.FC = () => {
  return <ProfileViews />
};

export default Profile;