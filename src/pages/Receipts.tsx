import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';
import ReceiptsViews from '../apps/ReceiptsViews';

const Receipts: React.FC = () => {

  return <ReceiptsViews />
};

export default Receipts;