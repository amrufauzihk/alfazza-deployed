import React from 'react';
import DashboardViews from '../apps/DashboardViews';

const Dashboard: React.FC = () => {
    return (
        <DashboardViews />
    );
};

export default Dashboard;
