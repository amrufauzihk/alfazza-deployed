import { IonButton, IonCard, IonCardContent, IonContent, IonHeader, IonImg, IonInput, IonLoading, IonPage, IonTitle, IonToolbar, useIonLoading, useIonRouter } from '@ionic/react';
import React from 'react';

const Login: React.FC = () => {
    const [showIonLoading, dismissIonLoading] = useIonLoading();
    const router = useIonRouter();

    const handleLogin = () => {
        showIonLoading('Loading')
        setTimeout(() => {
            dismissIonLoading();
            router.push('folder/Inbox')
        }, 4000);
    }

    return (
        <IonPage>
            {/* <IonLoading trigger="open-loading" message="Loading..." duration={3000} spinner="circles" /> */}
            <IonContent >
                <IonCard className='ion-align-items-center' style={{ height: '100vh', display: 'flex' }}>
                    <IonCardContent>
                        <IonImg
                            src='https://alfazzakreditsyariah.netlify.app/img/brand.png'
                        />
                        <IonTitle className='ion-text-center'>Alfazza Kredit Syariah</IonTitle>
                        <IonInput
                            label="Username"
                            type='text'
                            labelPlacement="floating"
                            fill="outline"
                            placeholder="Enter text"
                            className='ion-margin-top'
                        />
                        <IonInput
                            label="Password"
                            type='password'
                            labelPlacement="floating"
                            fill="outline"
                            placeholder="Enter text"
                            className='ion-margin-top'
                        />
                        <IonButton
                            className='ion-margin-top'
                            expand='block'
                            onClick={handleLogin}
                        >
                            Login
                        </IonButton>
                    </IonCardContent>
                </IonCard>
            </IonContent>
        </IonPage>
    );
};

export default Login;