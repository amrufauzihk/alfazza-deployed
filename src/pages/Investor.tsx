import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';
import InvestorViews from '../apps/InvestorViews';

const Investore: React.FC = () => {

  return <InvestorViews />
};

export default Investore;