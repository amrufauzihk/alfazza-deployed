import { IonButtons, IonCard, IonCardContent, IonCol, IonContent, IonGrid, IonHeader, IonInfiniteScroll, IonInfiniteScrollContent, IonMenuButton, IonPage, IonRow, IonSearchbar, IonSelect, IonSelectOption, IonTitle, IonToolbar } from '@ionic/react';
import { useParams } from 'react-router';
import { caretDownSharp } from 'ionicons/icons';
import { appPages } from '../constants/listMenu';
import { useCallback, useState } from 'react';
// import './Page.css';

const Page: React.FC = () => {
  const [items, setItems] = useState<string[]>([]);

  const generateItems = () => {
    const newItems = [];
    for (let i = 0; i < 50; i++) {
      newItems.push(`Item ${1 + items.length + i}`);
    }
    setItems([...items, ...newItems]);
  };

  const { name } = useParams<{ name: string; }>();
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="success">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>{name}</IonTitle>
        </IonToolbar>
        <IonToolbar>
          <IonGrid>
            <IonRow>
              <IonCol size="3">
                <IonSelect
                  toggleIcon={caretDownSharp}
                  interface="popover"
                  placeholder="Search By"
                >
                  <IonSelectOption value="apples">Name</IonSelectOption>
                  <IonSelectOption value="oranges">No KTP</IonSelectOption>
                  <IonSelectOption value="bananas">Anything</IonSelectOption>
                </IonSelect>
              </IonCol>
              <IonCol size="9" sizeLg='10'>
                <IonSearchbar></IonSearchbar>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonGrid>
          <IonRow>
            {[1,2,3,4,5,6,7,8,9,0,1,2,3,4].map(() => (
              <IonCol size="12">
                <IonCard>
                  <IonCardContent>
                    Testing
                  </IonCardContent>
                </IonCard>
              </IonCol>
            ))}
          </IonRow>
        </IonGrid>
        <IonInfiniteScroll
          onIonInfinite={(ev) => {
            generateItems();
            setTimeout(() => ev.target.complete(), 500);
          }}
        >
          <IonInfiniteScrollContent></IonInfiniteScrollContent>
        </IonInfiniteScroll>
      </IonContent>
    </IonPage>
  );
};

export default Page;
