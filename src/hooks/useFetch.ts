import customAxios from '../utils/customAxios';
import { EPStore } from '../utils/apiEndPoints';
import { AxiosRequestConfig } from 'axios';
import {
  QueryObserverBaseResult,
  QueryOptions,
  useQuery
} from '@tanstack/react-query';

type UseFetchQueryProps = {
  url: keyof typeof EPStore;
  options: {
    key: QueryOptions['queryKey'];
    urlParams?: string;
    options?: AxiosRequestConfig;
    lazy?: boolean;
    getAllResults?: boolean;
  };
};

export interface IResponseType<T> {
  data: T;
  current_page?: number;
  page_size?: number;
  count?: number;
  previous?: string;
  next?: string;
}

const useFetchQuery = <T>(
  url: UseFetchQueryProps['url'],
  options: UseFetchQueryProps['options']
): QueryObserverBaseResult<T> => {
  const { data, isLoading, isError, refetch, ...rest } = useQuery<T>({
    queryKey: [options.key],
    queryFn: async () => {
      const res = await customAxios.get(
        `${EPStore[url].url}${options?.urlParams ?? ''}`,
        options?.options
      );
      if (options.getAllResults) return res.data;

      return res.data.data;
    },
    enabled: !options.lazy ?? true,
    retry: false
  });

  return { data, isLoading, refetch, isError, ...rest };
};

export default useFetchQuery;
