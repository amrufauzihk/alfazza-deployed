'use client';

import { useState } from 'react';
import axios, { type AxiosError, type AxiosRequestConfig } from 'axios';

import customAxios from '../utils/customAxios';
import { EPStore } from '../utils/apiEndPoints';
import URLParamsReplacer from '../utils/URLParamsReplacer';

interface IResponse<T, E> {
	status?: string | number;
	message?: string;
	data?: T | E;
	code: number;
}

interface IUseMutation<T, E, V = null> {
	data: IResponse<T, E>['data'];
	error?: string;
	axiosError?: AxiosError<E>;
	loading: boolean;
	mutate: (v: IMutateFunction<V>) => Promise<IResponse<T, E>>;
}

interface OptionsType {
	options?: AxiosRequestConfig;
	urlParams?: string;
	urlParamObject?: Record<string, string | number>;
}

interface IMutateFunction<V> {
	body?: V | object;
	urlParams?: string;
	options?: AxiosRequestConfig;
}

const useMutation = <T, E = unknown>(url: keyof typeof EPStore, initOptions?: OptionsType): IUseMutation<T, E> => {
	const [data, setData] = useState<IUseMutation<T, E>['data']>();
	const [error, setError] = useState<IUseMutation<T, E>['error']>('');
	const [axiosError, setAxiosError] = useState<IUseMutation<T, E>['axiosError']>();
	const [loading, setLoading] = useState<IUseMutation<T, E>['loading']>(false);

	const mutate = async <V = object>({ body = {}, urlParams, options }: IMutateFunction<V>): Promise<IResponse<T, E>> => {
		setLoading(true);
		const isURL = EPStore[url].url;
		const isMethod = EPStore[url].method;

		const fallbackReturn = {
			status: 'error',
			message: 'An unknown error has occured',
			data: undefined as T | E | undefined,
			code: 555
		};

		try {
			const res = await customAxios[isMethod]<IResponse<T, E>>(
				URLParamsReplacer(`${isURL}${urlParams ?? ''}`, initOptions?.urlParamObject ?? {}),
				body,
				options
			);

			if (res) {
				if (res.data.status === 'error') {
					setError(res.data.message);
					setLoading(false);
					return res.data;
				}

				setLoading(false);
				setData(res.data.data);
				return res.data;
			}
		} catch (e) {
			setLoading(false);
			if (axios.isAxiosError<E>(e)) {
				setAxiosError(e);

				return {
					...(e.response?.data ?? fallbackReturn),
					code: +(e.response?.status ?? fallbackReturn.code)
				};
			}
			setError('An unknown error has occured');
		}

		// Fallback return
		return fallbackReturn;
	};

	return {
		data,
		loading,
		error,
		axiosError,
		mutate
	};
};

export default useMutation;
