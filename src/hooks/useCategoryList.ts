import { useEffect, useState } from 'react'
import { getAllData } from '../firebase/firebaseService';

const useCategoryList = () => {

  const [data, setData] = useState<CategoryProps[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const fetchCategoryList = async () => {
    setIsLoading(true);
    try {
      const res: any = await getAllData("category");
      if (res.length > 0) {
        setData(res);
      } else {
        setData([]);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchCategoryList();
  }, []);

  return {
    data, isLoading
  }
}

export default useCategoryList