"use client";

import { type ReactNode, type Dispatch, type SetStateAction } from "react";

import {
  MRT_ActionMenuItem,
  MRT_GlobalFilterTextField,
  MRT_ToggleFiltersButton,
  MaterialReactTable,
  useMaterialReactTable,
  type MRT_Cell,
  type MRT_ColumnDef,
  type MRT_Row,
  type MRT_RowData,
  type MRT_TableInstance,
  MRT_TableOptions,
  MRT_ColumnFiltersState,
} from "material-react-table";
import { Box, Button, Typography, useTheme } from "@mui/joy";
import { TableRowProps } from "@mui/material";
import { Download, Plus } from "lucide-react";

interface ICustomTableProps<T extends MRT_RowData> {
  columns: MRT_ColumnDef<T>[];
  data: T[];
  section: string;
  searchPlaceholder?: string;
  menu?: IMenuProps<T>[];
  hideAction?: boolean;
  onInsert?: () => void;
  onDownload?: () => void;
  columnFilters?: MRT_ColumnFiltersState;
  manualFiltering?: boolean;
  onBodyRowClick?:
    | TableRowProps
    | ((props: {
        isDetailPanel?: boolean | undefined;
        row: MRT_Row<T>;
        staticRowIndex: number;
        table: MRT_TableInstance<T>;
      }) => TableRowProps);
  enableRowSelection?: ((row: MRT_Row<T>) => boolean) | boolean;
  isLoading?: boolean;
  isStudentLectureData?: boolean;
  enableRowNumbers?: boolean;
  renderCustomActions?: (props: {
    cell: MRT_Cell<T>;
    row: MRT_Row<T>;
    staticRowIndex?: number;
    table: MRT_TableInstance<T>;
  }) => ReactNode;
  tableOptions?: MRT_TableOptions<T>;
  renderBulkActions?: (table: MRT_TableInstance<T>) => ReactNode;
  hideFilter?: boolean;
  hideSearch?: boolean;
}

interface IMenuProps<T extends MRT_RowData> {
  label: string;
  onClick: (row: MRT_Row<T>) => void;
  icon: ReactNode;
  disabled?: boolean;
}

interface IState {
  isLoading: boolean;
  columnFilters?: MRT_ColumnFiltersState;
}

function CustomTable<T extends MRT_RowData>({
  columns,
  data,
  section,
  searchPlaceholder = "Search",
  menu,
  hideAction,
  onInsert,
  onDownload,
  hideSearch,
  enableRowSelection,
  hideFilter = false,
  renderCustomActions,
  renderBulkActions,
  onBodyRowClick,
  manualFiltering,
  columnFilters,
  isLoading = false,
  enableRowNumbers = false,
  tableOptions,
}: ICustomTableProps<T>): ReactNode {
  const theme = useTheme();

  let state: IState = { isLoading };
  if (manualFiltering) state = { ...state, columnFilters };

  const table = useMaterialReactTable({
    enableColumnFilterModes: false,
    enableColumnOrdering: false,
    enableColumnActions: false,
    enableGrouping: false,
    enableColumnPinning: true,
    enableFacetedValues: false,
    enableRowActions: !hideAction,
    enableRowSelection,
    enablePagination: true,
    enableBottomToolbar: true,
    manualFiltering,
    enableSorting: false,
    enableRowNumbers,
    muiTableBodyRowProps: onBodyRowClick,
    initialState: {
      showColumnFilters: false,
      showGlobalFilter: true,
      columnPinning: {
        left: ["mrt-row-select"],
        right: ["mrt-row-actions"],
      },
      isLoading: false,
    },
    state,
    paginationDisplayMode: "pages",
    positionToolbarAlertBanner: "bottom",
    muiSearchTextFieldProps: {
      size: "small",
      variant: "outlined",
    },
    muiTablePaperProps: {
      sx: {
        boxShadow: "none",
        "& > .MuiBox-root": {
          boxShadow: "none",
        },
      },
    },
    muiTableBodyCellProps: {
      sx: {
        '&[data-pinned="true"]:before': {
          bgcolor: "white !important",
          boxShadow: "none",
        },
      },
    },
    muiSelectCheckboxProps: {
      sx: {
        color: "#D3D5D8",
      },
      ...tableOptions?.muiSelectCheckboxProps,
    },
    muiSelectAllCheckboxProps: {
      sx: {
        color: "#D3D5D8",
      },
      ...tableOptions?.muiSelectAllCheckboxProps,
    },
    muiTableHeadCellProps: {
      sx: {
        '&[data-pinned="true"]:before': {
          bgcolor: "white !important",
          boxShadow: "none",
        },
      },
    },
    muiTableBodyProps: {
      sx: {
        '&>tr[data-selected="true"]>td:after': {
          bgcolor: "#DCE9FB",
        },
        "&>tr:hover>td:after": {
          bgcolor: " #F6F7F8",
        },
      },
    },
    muiPaginationProps: {
      color: "standard",
      rowsPerPageOptions: [10, 20, 30],
      shape: "circular",
      variant: "outlined",
    },
    renderRowActions: renderCustomActions,
    renderRowActionMenuItems: ({ closeMenu, table, row }) => [
      menu &&
        menu.map((item) => (
          <MRT_ActionMenuItem
            key={item.label.toLocaleLowerCase()}
            onClick={() => {
              item.onClick(row);
              closeMenu();
            }}
            disabled={item.disabled}
            label={item.label}
            icon={item.icon}
            table={table}
          />
        )),
    ],
    renderTopToolbar: ({ table }) => {
      if (hideSearch && hideFilter) return <></>;
      return (
        <Box
          sx={{
            backgroundColor: "white",
            display: "flex",
            gap: "0.5rem",
            pt: 1,
            pb: 3,
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box sx={{ display: "flex", gap: "0.5rem", alignItems: "center" }}>
            {!hideSearch && (
              <MRT_GlobalFilterTextField
                sx={{
                  borderRadius: 2,
                  width: 320,
                }}
                placeholder={searchPlaceholder}
                table={table}
              />
            )}
            {!hideFilter && <MRT_ToggleFiltersButton table={table} />}
            {(table.getIsSomeRowsSelected() || table.getIsAllRowsSelected()) &&
              renderBulkActions && (
                <Box sx={{ display: "flex", gap: "0.5rem" }}>
                  {renderBulkActions(table)}
                </Box>
              )}
          </Box>

          <Box display="flex" flexDirection="row" gap={2}>
            {onDownload && (
              <Button
                variant="outlined"
                onClick={onDownload}
                color="primary"
                startDecorator={<Download />}
              >
                Download
              </Button>
            )}
            {onInsert && (
              <Button
                variant="solid"
                onClick={onInsert}
                color="success"
                startDecorator={<Plus />}
              >
                Insert
              </Button>
            )}
          </Box>
        </Box>
      );
    },
    renderEmptyRowsFallback: () => {
      return (
        <Box
          my={5}
          display="flex"
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
        >
          <Box>
            <img
              src="https://alfazzakreditsyariah.netlify.app/img/brand.png"
              width={120}
              height={48}
              alt="No records to display"
            />
          </Box>
          <Box>
            <Typography textColor="text.primary" fontWeight={600}>
              No {section} Data Available
            </Typography>
          </Box>
          <Box>
            <Typography>{section} data will be shown here.</Typography>
          </Box>
        </Box>
      );
    },
    localization: {
      actions: "Action",
      rowsPerPage: "Show",
      rowNumber: "No.",
    },
    columns: columns,
    data,
    ...tableOptions,
  });

  return <MaterialReactTable table={table} />;
}

export default CustomTable;
