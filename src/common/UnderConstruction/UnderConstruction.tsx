import { IonTitle, IonText, IonImg, IonContent } from '@ionic/react';
import React from 'react';

import './UnderConstruction.css';

const UnderConstruction: React.FC = () => {

  return (
    <IonContent className='ion-padding ion-text-center'>
      <IonImg src='https://cdn.dribbble.com/users/1603428/screenshots/4158745/web-dev-gif.gif' />
      <IonTitle>Under Construction</IonTitle>
      <IonText>Sedang Dalam Pengembangan</IonText>
    </IonContent>
  );
};

export default UnderConstruction;