import React, { useState } from "react";
import { menuController } from "@ionic/core/components";
import {
  IonAlert,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonMenuButton,
  IonModal,
  IonProgressBar,
  IonTitle,
  IonToolbar,
  useIonLoading,
  useIonRouter,
} from "@ionic/react";
import {
  closeOutline,
  logOutOutline,
  personCircleOutline,
  personOutline,
  settingsOutline,
} from "ionicons/icons";

import "./MainHeaders.css";

interface HeadersProps {
  title: string;
  loading?: boolean;
}

const MainHeaders: React.FC<HeadersProps> = ({
  title = "Page Title",
  loading = false,
}) => {
  const router = useIonRouter();
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [showIonLoading, dismissIonLoading] = useIonLoading();

  const handleLogout = () => {
    showIonLoading("Loading");
    setTimeout(() => {
      setModalOpen(false);
      dismissIonLoading();
      /** TECHDEBT: REMOVE CODE SHOULD BE HERE */
      router.push("/login");
    }, 4000);
  };

  return (
    <IonHeader className="">
      <IonToolbar>
        <IonButtons slot="start">
          <IonMenuButton />
        </IonButtons>

        {loading && <IonProgressBar type="indeterminate"></IonProgressBar>}
        <IonButtons slot="end" className="ion-padding">
          <IonButton onClick={() => setModalOpen(true)}>
            <IonIcon color="primary" icon={personCircleOutline} size="large" />
          </IonButton>
        </IonButtons>
        <IonModal mode="ios" isOpen={modalOpen}>
          <IonHeader>
            <IonToolbar>
              {/* <IonTitle>Notification</IonTitle> */}
              <IonButtons slot="end">
                <IonButton onClick={() => setModalOpen(false)}>
                  <IonIcon icon={closeOutline} size="large" />
                </IonButton>
              </IonButtons>
            </IonToolbar>
          </IonHeader>
          <IonContent className="ion-padding">
            <IonButton
              color={`light`}
              expand="full"
              onClick={() => {
                setModalOpen(false);
                router.push("/profile");
              }}
            >
              <IonIcon slot="start" icon={personOutline} />
              Profile
            </IonButton>
            <IonButton
              color={`light`}
              expand="full"
              onClick={() => {
                setModalOpen(false);
                router.push("/profile");
              }}
            >
              <IonIcon slot="start" icon={settingsOutline} />
              Pengaturan
            </IonButton>
            <IonButton
              id="logout-section"
              color={`danger`}
              expand="full"
              onClick={async () => {
                await menuController.close();
              }}
            >
              <IonIcon slot="start" icon={logOutOutline} />
              Keluar
            </IonButton>
            <IonAlert
              header="Apakah anda ingin keluar ?"
              trigger="logout-section"
              mode="ios"
              buttons={[
                {
                  text: "Tidak",
                  role: "cancel",
                },
                {
                  text: "Keluar",
                  role: "confirm",
                  handler: handleLogout,
                },
              ]}
            ></IonAlert>
          </IonContent>
        </IonModal>
      </IonToolbar>
    </IonHeader>
  );
};

export default MainHeaders;
