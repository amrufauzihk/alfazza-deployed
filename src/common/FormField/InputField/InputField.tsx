import React from "react";
import {
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  InputProps,
  Typography,
} from "@mui/joy";

interface InputFieldProps extends InputProps {
  label: string;
  placeholder?: string;
  helperText?: string;
  errorText?: string;
  required?: boolean;
}

const InputField: React.FC<InputFieldProps> = ({
  label,
  placeholder,
  helperText,
  errorText,
  required,
  ...props
}) => {
  return (
    <FormControl error={!!errorText}>
      <FormLabel>
        {label}
        {required && <Typography color="danger">*</Typography>}
      </FormLabel>
      <Input placeholder={placeholder} required={required} {...props} />
      {!!(errorText || helperText) && (
        <FormHelperText>{errorText ?? helperText}</FormHelperText>
      )}
    </FormControl>
  );
};

export default InputField;
