import React from "react";
import {
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  InputProps,
} from "@mui/joy";

interface TextAreaFieldProps extends InputProps {
  label: string;
  placeholder?: string;
  helperText?: string;
  errorText?: string;
  required?: boolean;
}

const TextAreaField: React.FC<TextAreaFieldProps> = ({
  label,
  placeholder,
  helperText,
  errorText,
  required,
  ...props
}) => {
  return (
    <FormControl error={!!errorText}>
      <FormLabel>{label}</FormLabel>
      <Input placeholder={placeholder} required={required} {...props} />
      {!!(errorText || helperText) && (
        <FormHelperText>{errorText ?? helperText}</FormHelperText>
      )}
    </FormControl>
  );
};

export default TextAreaField;
