import {
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  InputProps,
} from "@mui/joy";
import React from "react";
import NumericFormatAdapter from "../../NumericFormatAdapter/NumericFormatAdapter";

interface CurrencyFieldProps extends InputProps {
  label: string;
  placeholder?: string;
  helperText?: string;
  errorText?: string;
  required?: boolean;
}

const CurrencyField: React.FC<CurrencyFieldProps> = ({
  label,
  placeholder,
  helperText,
  errorText,
  required,
  ...props
}) => {
  return (
    <FormControl error={!!errorText}>
      <FormLabel>{label}</FormLabel>
      <Input
        required={required}
        placeholder={placeholder}
        slotProps={{
          input: {
            component: NumericFormatAdapter,
          },
        }}
        {...props}
      />
      {!!(errorText || helperText) && (
        <FormHelperText>{errorText ?? helperText}</FormHelperText>
      )}
    </FormControl>
  );
};

export default CurrencyField;
