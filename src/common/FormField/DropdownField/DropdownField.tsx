import {
  FormControl,
  FormHelperText,
  FormLabel,
  Option,
  Select,
  SelectProps,
  Typography,
} from "@mui/joy";
import React, { Fragment } from "react";

interface IDropdownField extends SelectProps<OptionType["value"], false> {
  options: OptionType[];
  label: string;
  isAutocomplete?: boolean;
  value: any;
  name: string;
  required?: boolean;
  formFieldProps?: any;
  placeholder?: string;
  disabled?: boolean;
  errorText?: any;
  helperText?: any;
  color?: any;
}

const DropdownField: React.FC<IDropdownField> = ({
  options,
  label,
  value,
  required,
  formFieldProps,
  placeholder,
  isAutocomplete,
  disabled,
  name,
  errorText,
  helperText,
  color,
  ...props
}) => {
  return (
    <FormControl error={!!errorText}>
      <FormLabel>
        {label} {required && <Typography color="danger">*</Typography>}
      </FormLabel>
      <Select
        name={name}
        value={value}
        disabled={disabled}
        placeholder={placeholder}
        {...props}
      >
        {options?.map((v, i) => (
          <Fragment key={i}>
            <Option value={v.value}>{v.label}</Option>
          </Fragment>
        ))}
      </Select>
      {!!(errorText || helperText) && (
        <FormHelperText>{errorText ?? helperText}</FormHelperText>
      )}
    </FormControl>
  );
};

export default DropdownField;
