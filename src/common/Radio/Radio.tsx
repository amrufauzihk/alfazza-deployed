import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React, { ChangeEvent } from 'react';
import cx from 'classnames';

import './Radio.css';

interface RadioProps {
  id?: string;
  label: string;
  className?: string;
  classNameInput?: string;
  classNameLabel?: string;
  name?: string;
  defaultValue: string;
  selectedValue?: string;
  register?: any;
  onOptionChange?: (e: ChangeEvent<HTMLInputElement>) => void;
}

const Radio: React.FC<RadioProps> = ({
    id = null,
    label,
    className = '',
    defaultValue = '',
    classNameLabel = '',
    classNameInput = '',
    register = undefined,
    name,
    selectedValue,
    onOptionChange = () => {},
  }) => {
  return (
    <div className={cx(className, 'radio')}>
      <input
        type="radio"
        id={id || defaultValue}
        value={defaultValue}
        className={classNameInput}
        {...(register ? { ...register(name) } : { name, checked: selectedValue === defaultValue, onChange: onOptionChange })}
      /> <label className={cx(classNameLabel, 'radio-label')} htmlFor={id || defaultValue}>{label}</label>
    </div>
  );
};

export default Radio;