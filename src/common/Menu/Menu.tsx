import { useLocation } from "react-router-dom";
import {
  IonBadge,
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonNote,
  useIonRouter,
} from "@ionic/react";
import {
  albums,
  albumsOutline,
  albumsSharp,
  bag,
  bagOutline,
  calendar,
  calendarOutline,
  card,
  cardOutline,
  expand,
  expandOutline,
  home,
  homeOutline,
  key,
  keyOutline,
  paperPlaneOutline,
  paperPlaneSharp,
  personAdd,
  personAddOutline,
  thunderstorm,
  thunderstormOutline,
} from "ionicons/icons";
import "./Menu.css";

interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
  components?: any;
  disable?: boolean;
}

const appPages: AppPage[] = [
  {
    title: "Dashboard",
    url: "dashboard",
    iosIcon: homeOutline,
    mdIcon: home,
  },
  {
    title: "Sales",
    url: "sales",
    iosIcon: bagOutline,
    mdIcon: bag,
  },
  {
    title: "Purchase",
    url: "purchase",
    iosIcon: cardOutline,
    mdIcon: card,
  },
  {
    title: "Receipts",
    url: "receipt",
    iosIcon: paperPlaneOutline,
    mdIcon: paperPlaneSharp,
  },
  {
    title: "Expense",
    url: "expense",
    iosIcon: expandOutline,
    mdIcon: expand,
  },
  {
    title: "Investor",
    url: "investor",
    iosIcon: personAddOutline,
    mdIcon: personAdd,
  },
  {
    title: "Category & Merk",
    url: "category",
    iosIcon: albumsOutline,
    mdIcon: albums,
  },
  {
    title: "Report",
    url: "report",
    disable: true,
    iosIcon: thunderstormOutline,
    mdIcon: thunderstorm,
  },
  {
    title: "Calendar",
    url: "calendar",
    disable: true,
    iosIcon: calendarOutline,
    mdIcon: calendar,
  },
  {
    title: "Master COA",
    url: "mastercoa",
    disable: true,
    iosIcon: keyOutline,
    mdIcon: key,
  }
];

const PREVENT_SHOW_SIDEBAR = ["/login", "/register"];

const Menu: React.FC = () => {
  const location = useLocation();
  const router = useIonRouter();
  
  return (
    <IonMenu
      contentId="main"
      type="overlay"
      disabled={PREVENT_SHOW_SIDEBAR.includes(router.routeInfo.pathname)}
    >
      <IonContent>
        <IonList id="inbox-list">
          <img
            style={{ width: "150px", borderRadius: "50%" }}
            src="https://alfazzakreditsyariah.netlify.app/img/brand.png"
            className="imageMenuBar"
          />
          <IonListHeader>Muhammad Aryandi</IonListHeader>
          <IonNote>aryandi@alfazzakreditsyariah.com</IonNote>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem
                  disabled={appPage.disable}
                  className={
                    location.pathname === `/${appPage.url}` ? "selected" : ""
                  }
                  routerLink={appPage.url}
                  routerDirection="none"
                  lines="none"
                  detail={false}
                >
                  <IonIcon
                    color={
                      location.pathname === `/${appPage.url}` ? "selected" : ""
                    }
                    aria-hidden="true"
                    slot="start"
                    ios={appPage.iosIcon}
                    md={appPage.mdIcon}
                  />
                  <IonLabel>
                    {appPage.title}{" "}
                    {appPage.disable && (
                      <IonBadge color={`danger`}>TBD</IonBadge>
                    )}
                  </IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
