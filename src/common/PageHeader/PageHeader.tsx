import React from "react";
import { IonContent } from "@ionic/react";

import "./PageHeader.css";

interface PageHeaderProps {
  title: string;
  description?: string;
}

const PageHeader: React.FC<PageHeaderProps> = ({
  title = "Page Title",
  description = false,
}) => {
  return (
    <div className="header-page">
      <h1>{title}</h1>
      <span className="ion-h8">{description}</span>
    </div>
  );
};

export default PageHeader;
