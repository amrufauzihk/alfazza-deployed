export const INIT_PURCHASE = {
    hargaBarang: '',
    jenisBarang: '',
    merk: '',
    tanggalTransaksi: '',
    type: '',
    kindOfTransaction: 'Inventory',
    potongan: '',
    umurEkonomis: '',
    nilaiSisa: ''
}