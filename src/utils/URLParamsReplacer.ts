import { type EPStore } from './apiEndPoints';

export const replaceStringInBrackets = (originalString: keyof typeof EPStore, stringName: string, replacementValue: string | number): string => {
	const pattern = new RegExp(`{${stringName}}`, 'g');
	return (originalString as string).replace(pattern, `${replacementValue}`);
};

const URLParamsReplacer = (url: keyof typeof EPStore, params: Record<string, string | number>): string => {
	for (const k in params) {
		if (Object.prototype.hasOwnProperty.call(params, k)) {
			url = replaceStringInBrackets(url, k, params[k]);
		}
	}
	return url as string;
};

export default URLParamsReplacer;
