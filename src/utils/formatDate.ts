const defaultOptions: Intl.DateTimeFormatOptions = {
  day: "numeric",
  month: "long",
  year: "numeric",
};

export const formatDate = (
  inputDate: string,
  options: Intl.DateTimeFormatOptions = defaultOptions,
  localization: "en-US" | "id" = "en-US"
) => {
  if (inputDate) {
    return new Date(inputDate).toLocaleDateString(localization, options);
  }
  return ""
};

export const formatDateTime = (
  inputDate: string,
  options: Intl.DateTimeFormatOptions = defaultOptions,
  localization: "en-US" | "id" | "en-GB" = "en-US"
) => {
  console.log("inputDate", inputDate);
  if (inputDate) {
    const date = new Date(inputDate);
    console.log("formatDateTime", date);
    
    return new Intl.DateTimeFormat(localization, options).format(date)
  }
  return "";
}

export const formatInputDate = (value: string) =>
  new Date(new Date(value)).toISOString();

export const formatDateFromInput = (inputDate: string): string => {
  // Create a Date object from the input date string
  const dateObj = new Date(inputDate);

  // Get day, month, and year from the date object
  const day = dateObj.getDate();
  const monthIndex = dateObj.getMonth();
  const year = dateObj.getFullYear();

  // Array of month names
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  // Format the date
  const formattedDate = `${day} ${monthNames[monthIndex]} ${year}`;

  return formattedDate;
};
