export const Window: (Window & typeof globalThis) | undefined =
  typeof window === 'undefined' ? undefined : window;
