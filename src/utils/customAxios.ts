'use client';
import cookies from 'react-cookies';
import axios, { AxiosError, AxiosHeaders, AxiosRequestConfig, RawAxiosRequestHeaders } from 'axios';

type AccessToken = {
  access: string;
  refresh: string;
};
interface CustomAxiosRequestConfig extends AxiosRequestConfig {
  _retry?: boolean;
}

const BASE_URL = `http://localhost:1337`;
const homeURL = `http://localhost:5173`;

const accessToken = cookies.load('pkaccesstoken');
const refreshToken = cookies.load('pkrefreshtoken');

export const configCookies = {
  domain: homeURL,
  path: '/',
  expires: new Date('9999-12-31T12:00:00.000Z')
};

export const logout = async () => {
  if (refreshToken) {
    try {
      await axios.post(
        `${BASE_URL}['logout'].url}`,
        {},
        {
          headers: {
            Authorization: accessToken
          }
        }
      );
    } catch (error) {
      console.log('API Logout Error:', error);
    }
  }

  cookies.remove('lang', configCookies);
  cookies.remove('pkaccesstoken', configCookies);
  cookies.remove('pkrefreshtoken', configCookies);
  cookies.remove('pktokenexpiry', configCookies);
  cookies.remove('pklogintimestamp', configCookies);
  cookies.remove('pkusername', configCookies);
  cookies.remove('pkuserdetails', configCookies);
  cookies.remove('pkuniversitydata', configCookies);
  cookies.remove('personalField', configCookies);
  
  // example using localstorage
  // Window?.localStorage.removeItem('pkuserdetails');

  // Checking if the user is logging out and was originated from custom region site
  // Redirect to the website the user initial logged in region
  // Window?.open(`https://vpn.${homeURL}/login`, '_self');
};

const customAxios = axios.create({
  baseURL: BASE_URL,
  headers: {
    Accept: 'application/json, text/plain, */*'
  }
});

customAxios.interceptors.request.use(
  async (config) => {
    if (!accessToken || !refreshToken) {
      await logout();
    }
    config.headers = {
      'Content-Type': 'application/json',
      ...(accessToken && { Authorization: `Bearer ${accessToken}` }),
      ...config.headers
    };
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

customAxios.interceptors.response.use(
  async (response) => {
    return response;
  },
  async function (e) {
    const error = e as AxiosError;
    const originalRequest = error.config as CustomAxiosRequestConfig;

    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401 && accessToken) {
        logout();
      }
    }

    return Promise.reject(error);
  }
);

export default customAxios;
