import { Window } from './window';

export const homeURL = (() => {
  let url = Window?.location.href.split('/')[2];
  let splitUrl = url?.split('.');

  if (splitUrl?.[2] === 'portalkampus') {
    return `portalkampus.co.id`;
  } else if (splitUrl?.[2] === 'vertical') {
    return `vertical.id`;
  } else {
    return `localhost`;
  }
})();

export const baseURL = (() => {
  let url = Window?.location.href.split('/')[2];
  let splitUrl = url?.split('.');

  if (splitUrl?.[2] === 'portalkampus') {
    return `portalkampus.co.id`;
  } else {
    return `vertical.id`;
  }
})();

export const EPStore: {
  [key: string]: {
    url: string;
    method: 'get' | 'post' | 'put' | 'delete' | 'patch';
  };
} = {
  getListCategoryHelpCenter: {
    url: `/api/v1/help/category/list`,
    method: 'get'
  },
  getSearchResultHelpCenter: {
    url: `/api/v1/help/search-data`,
    method: 'get'
  },
  getListCategoryDetailHelpCenterFAQ: {
    url: `/api/v1/help/faq/list`,
    method: 'get'
  },
  getListCategoryDetailContentHelpCenterFAQ: {
    url: `/api/v1/help/faq`,
    method: 'get'
  },
  getListCategoryDetailHelpCenterVideoTutorial: {
    url: `/api/v1/help/video/list`,
    method: 'get'
  },
  getListCategoryDetailContentHelpCenterVideoTutorial: {
    url: `/api/v1/help/video`,
    method: 'get'
  },
  getDashboard: {
    url: '/api/v1/dashboard',
    method: 'get'
  },
  getRoleAndPermissions: {
    url: '/api/v1/role-and-permissions/',
    method: 'get'
  },
  getListEnums: {
    url: '/api/v1/enum/',
    method: 'get'
  },
  refreshToken: {
    url: '/api/v1/user/auth/refresh/',
    method: 'post'
  },
  logout: {
    url: '/api/v1/user/auth/logout/',
    method: 'post'
  },
  getListUsers: {
    url: '/api/v1/user/list/',
    method: 'get'
  },
  changePassword: {
    url: '/api/v1/user/change-password/',
    method: 'post'
  },
  fetchUser: {
    url: '/api/v1/user/',
    method: 'get'
  },
  updateUser: {
    url: '/api/v1/user/',
    method: 'patch'
  },
  getExportCredentials: {
    url: '/api/v1/user/list/export/credentials/',
    method: 'get'
  },
  getDownloadTemplate: {
    url: '/api/v1/user/list/import/',
    method: 'get'
  },
  postImportData: {
    url: '/api/v1/user/list/import/',
    method: 'post'
  },
  getListFaculties: {
    url: '/api/v1/institute/faculties/',
    method: 'get'
  },
  getListUnit: {
    url: '/api/v1/institute/unit/',
    method: 'get'
  },
  getListMajor: {
    url: '/api/v1/institute/major/',
    method: 'get'
  },
  getListPermissions: {
    url: '/api/v1/role-and-permissions/permissions/',
    method: 'get'
  },
  createRolePermissions: {
    url: '/api/v1/role-and-permissions/create/',
    method: 'post'
  },
  updateRolePermissions: {
    url: '/api/v1/role-and-permissions/update/',
    method: 'patch'
  },
  switchRolePermissions: {
    url: '/api/v1/role-and-permissions/switch/',
    method: 'patch'
  },
  deleteRolePermissions: {
    url: '/api/v1/role-and-permissions/delete/',
    method: 'delete'
  },
  createUserManagement: {
    url: '/api/v1/user/create/',
    method: 'post'
  },
  detailUserManagement: {
    url: '/api/v1/user/detail/',
    method: 'get'
  },
  deleteUserManagement: {
    url: '/api/v1/user/detail/',
    method: 'delete'
  },
  updateUserManagement: {
    url: '/api/v1/user/update-by-admin/',
    method: 'patch'
  },
  getListCustomFields: {
    url: '/api/v1/custom-fields/',
    method: 'get'
  },
  createCustomFields: {
    url: '/api/v1/custom-fields/create/',
    method: 'post'
  },
  updateCustomFields: {
    url: '/api/v1/custom-fields/update/',
    method: 'patch'
  },
  deleteCustomFields: {
    url: '/api/v1/custom-fields/delete/',
    method: 'delete'
  },
  createUpdateCustomFields: {
    url: '/api/v1/custom-fields/create-or-update/',
    method: 'post'
  },
  deleteChoiceCustomFields: {
    url: '/api/v1/custom-fields/choices/delete/',
    method: 'delete'
  },
  getExportStudent: {
    url: '/api/v1/user/list/export/students/',
    method: 'get'
  },
  getExportLecturer: {
    url: '/api/v1/user/list/export/lecturers/',
    method: 'get'
  },
  getUserRoles: {
    url: '/api/v1/user/roles/',
    method: 'get'
  },
  userBulkAction: {
    url: '/api/v1/user/bulk-actions/',
    method: 'post'
  },
  getListAcademicSupervisor: {
    url: '/api/v1/academic-supervisor/',
    method: 'get'
  },
  createAcademicSupervisor: {
    url: '/api/v1/academic-supervisor/create/',
    method: 'post'
  },
  deleteAcademicSupervisor: {
    url: '/api/v1/academic-supervisor/delete/',
    method: 'delete'
  },
  getDetailAcademicSupervisor: {
    url: '/api/v1/academic-supervisor/retrieve-supervisor/',
    method: 'get'
  },
  getListStudentDetailAcademicSupervisor: {
    url: '/api/v1/academic-supervisor/retrieve-supervisor/list-students/',
    method: 'get'
  },
  getStudentsDetailAcademicSupervisor: {
    url: '/api/v1/academic-supervisor/students/',
    method: 'get'
  },
  patchUpdateMembersAcademicSupervisor: {
    url: '/api/v1/academic-supervisor/update-members/',
    method: 'patch'
  },
  getListBatch: {
    url: '/api/v1/academic/batch/',
    method: 'get'
  },
};
