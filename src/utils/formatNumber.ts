export function formatCurrency(input?: string | number): string {
    if (input) {
        // Convert input to number
        const number: number = typeof input === 'string' ? parseFloat(input) : input;
        
        // Check if the input is a valid number
        if (isNaN(number)) {
            return 'Invalid input';
        }
    
        // Format number to IDR currency
        const formattedNumber: string = new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(number);
        return formattedNumber;
    }
    return '';
}